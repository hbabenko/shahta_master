#pragma strict

private var freezeMouseLook :boolean; 
private var freezeArrowMove :boolean;


function Start () {
	//by default its not moving 
	freezeMouseLook = true;
	freezeArrowMove = true;
}

function LateUpdate() {

 		var person : GameObject;
 		var controller : GameObject;
 		var ySpeed :int =10;
     	person = GameObject.Find("First Person Controller/Main Camera");
     	controller = GameObject.Find("First Person Controller");
     	var controllermove=controller.GetComponent(CharacterMotor);
     	var y = person.transform.rotation.y;
     	var x = person.transform.rotation.x;
     	var z = person.transform.rotation.z;
    if(Input.GetMouseButtonDown(1))
	 {
        Debug.Log("Pressed left click."+x+y+z);
        freezeMouseLook=!freezeMouseLook;
        var move = person.GetComponent(MouseLook);
     	if (freezeMouseLook)
     	{
        	//Freeze Mouse Look
        	move.enabled =  false;
        	 //person.transform.rotation = Quaternion.Euler(0, 0, 0);
     	}
     	else
     	{
     		//Enable Mouse Look
     		move.enabled =  true;
     	} 
     	Debug.Log("___WOW__"); 
        //Make Person Movble or Freeze it
     }
     var factor = 0.04f; // change this depending on interval to move camera 
  	if(Input.GetKey(KeyCode.LeftArrow)){
  		if(freezeArrowMove){
  			controllermove.enabled = false;
  		}
  	 	y +=ySpeed * factor;
  	 	Debug.Log("Coordinate y:" +y);
  	 	//person.transform.rotation = Quaternion.Euler(0, y, 0); 
  	 	controller.transform.Rotate(0, -y, 0);
  	 	}
    else if(Input.GetKey(KeyCode.RightArrow)) {
    	if(freezeArrowMove){
  			controllermove.enabled = false;
  		}
  		Debug.Log("Coordinate y:" +y);
    	y += ySpeed * factor;
    	//person.transform.rotation = Quaternion.Euler(0, y, 0); 
    	controller.transform.Rotate(0, y, 0);
    	}
    else if(Input.GetKey(KeyCode.UpArrow)) {
    	if(freezeArrowMove){
  			controllermove.enabled = true;
  		}
     }
    else if(Input.GetKey(KeyCode.DownArrow)) {
    	if(freezeArrowMove){
  			controllermove.enabled = true;
  		}
      }
}