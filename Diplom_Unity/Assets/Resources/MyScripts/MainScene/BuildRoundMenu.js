#pragma strict

var questionsCount : int;
var customGuiStyle : GUIStyle;
var customGuiStyleCenter : GUIStyle;


var currentObject : GameObject;

var variant1: String;
var variant2: String;
var variant3: String;
var variant4: String;
var variant5: String;
var variant6: String;

public var ObjectUniqueNumber : int;
/*function Start () {

}

function Update () {
	 
	 
	 if(Input.GetMouseButton(0))
	 {
        Debug.Log("Pressed left click.");
     }
    
}*/
function OnGUI () {
    // Provide the name of the Style as the final argument to use it
    //GUILayout.Button ("Cyrillyc Text: -Тест Русского Языка- 1", customGuiStyle);
	
	var pointX : float = Screen.width/1024.0;
    var pointY : float = Screen.height/768.0;
    
    var CenterX :float = Screen.width/2;
    var CenterY :float = Screen.height/2;
    print("questions" + questionsCount);
    var buttonSelected =0;
    var Menu = currentObject.GetComponent(AnswerMenu);
    Menu.objectN = ObjectUniqueNumber;
	GUI.Label(Rect((CenterX - 40*pointX),(CenterY - 40*pointY),(80*pointX),(80*pointY)), "", customGuiStyleCenter);
	
	//var problem = GameObject.FindWithTag("Problem");
	
	if (GUI.Button (Rect ((CenterX + 150*pointX),(CenterY - 110*pointY),(300*pointX),(100*pointY)), variant1, customGuiStyle)) {
        
        buttonSelected =1;
        print ("You clicked the button! next" + buttonSelected);
        
     	this.enabled = false;
     	Menu.buttonSelected = buttonSelected;
     	Menu.title = "Не работает датчик ДМТ";
     	Menu.variants=[];
        Menu.variants.push("1.обрыв кабеля");
        Menu.variants.push("2.отключен или неисправность в системе АГЗ");
        Menu.variants.push("");
        Menu.variants.push("");
        Menu.rightAnswer=1;
    	Menu.enabled = true;
    }
    if (GUI.Button (Rect ((CenterX + 150*pointX),(CenterY + 10*pointY),(300*pointX),(100*pointY)), variant2, customGuiStyle)) {
          buttonSelected =2;
        print ("You clicked the button! next" + buttonSelected);
     	this.enabled = false;
     	Menu.buttonSelected = buttonSelected;
     	Menu.title = "Допустимая концентрация метана контролируемая в данном месте?";
     	Menu.variants=[];
        Menu.variants.push("1.-	0,5%");
        Menu.variants.push("2.-	1,0%");
        Menu.variants.push("3.-	1,5%");
        Menu.variants.push("4.-	2,0%");
        Menu.rightAnswer=2;
        
   	    Menu.enabled = true;
    }
    if (GUI.Button (Rect ((CenterX - 150*pointX),(CenterY + 130*pointY),(300*pointX),(100*pointY)), variant3, customGuiStyle)) {
          buttonSelected =3;
        print ("You clicked the button! next" + buttonSelected);
     	this.enabled = false;
     	Menu.buttonSelected = buttonSelected;
     	Menu.title = "Какое правильное расположение датчика ДМТ по высоте выработки?";
     	Menu.variants=[];
        Menu.variants.push("1.под. кровлей выработки");
        Menu.variants.push("2.посредине высоты выработки");
        Menu.variants.push("3.посредине выработки у стенки");
        Menu.variants.push("4.у почвы выработк");
        Menu.rightAnswer=1;
     	
   	 	Menu.enabled = true;
    }
     if (GUI.Button (Rect ((CenterX - 150*pointX),(CenterY - 210*pointY),(300*pointX),(100*pointY)), variant4, customGuiStyle)) {
          buttonSelected =4;
        print ("You clicked the button! next" + buttonSelected);
     	this.enabled = false;
     	Menu.buttonSelected = buttonSelected;
     	
     	Menu.title = "Какое правильное расположение датчика ДМТ по отношению к вентиляционному потоку?";
     	Menu.variants=[];
        Menu.variants.push("1.вентиляционный поток набегает на лицевую панель");
        Menu.variants.push("2.вентиляционный поток набегает на боковую панель");
        Menu.variants.push("3.вентиляционный поток набегает на заднюю панель");
        Menu.variants.push("4.вентиляционный поток набегает на нижнюю панель");
        Menu.rightAnswer=3;
     	
   		Menu.enabled = true;
    }
     if (GUI.Button (Rect ((CenterX - 450*pointX),(CenterY- 110*pointY),(300*pointX),(100*pointY)), variant5, customGuiStyle)) {
         buttonSelected =5;
        print ("You clicked the button! next" + buttonSelected);
     	this.enabled = false;
     	Menu.buttonSelected = buttonSelected;
     
     	Menu.title = "Какое правильное расположение  датчика ДМТ  относительно вентиляционного трубопровода?";
     	Menu.variants=[];
        Menu.variants.push("1.на стороне противоположной вентиляционному трубопроводу");
        Menu.variants.push("2.под вентиляционным трубопроводом на стенке");
        Menu.variants.push("3.под вентиляционным трубопроводом на почве");
        Menu.variants.push("4.над вентиляционным трубопроводом под. кровлей");
        Menu.rightAnswer=1;
    	Menu.enabled = true;
    }
     if (GUI.Button (Rect ((CenterX - 450*pointX),(CenterY +10*pointY),(300*pointX),(100*pointY)), variant6, customGuiStyle)) {
         buttonSelected =6;
        print ("You clicked the button! next" + buttonSelected);
     	this.enabled = false;
     	Menu.buttonSelected = buttonSelected;
     	Menu.title = "Какое правильное расположение  датчика ДМТ относительно сопряжения тупиковой выработки?";
     	Menu.variants=[];
        Menu.variants.push("1.на расстоянии 5 м от сопряжения");
        Menu.variants.push("2.на расстоянии 20-30 м от сопряжения");
        Menu.variants.push("3.на расстоянии 10-20 м от сопряжения");
        Menu.variants.push("4.датчик тут не нужен");
        Menu.rightAnswer=3;  
   		Menu.enabled = true;
    }
    
    //problem.active = false;
    
    // If you do not want to apply the Style, do not provide the name
    //GUILayout.Button ("I am a normal UnityGUI Button without custom style -Русский текст без стиля-");
    
    
}
