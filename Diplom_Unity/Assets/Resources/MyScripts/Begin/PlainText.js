/* Declare a GUI Style */
var customGuiStyle : GUIStyle;
var universityLine = "ДОНЕЦКИЙ НАЦИОНАЛЬНЫЙ ТЕХНИЧЕСКИЙ УНИВЕРСИТЕТ";
var cafedraLine = "КАФЕДРА ОХРАНЫ ТРУДА";

var nameLabLine = " СИМУЛЯТОР: \"Виртуальная шахта\" ";

var nameStudentLine = " Выполнил(а):";
var nameGroupLine = "Группа:";

var bottomLine = "Донецк -2012";


function OnGUI () {
    // Provide the name of the Style as the final argument to use it
    //GUILayout.Button ("Cyrillyc Text: -Тест Русского Языка- 1", customGuiStyle);
	var Widht : float = Screen.width/1024.0;
    var Heigh : float = Screen.height/768.0;
    
	GUI.Label(Rect((200*Widht),(30*Heigh),(600*Widht),(50*Heigh)), universityLine, customGuiStyle);
	GUI.Label(Rect((200*Widht),(70*Heigh),(600*Widht),(50*Heigh)), cafedraLine, customGuiStyle);
	GUI.Label(Rect((200*Widht),(320*Heigh),(600*Widht),(50*Heigh)), nameLabLine, customGuiStyle);
	
	GUI.Label(Rect((500*Widht),(420*Heigh),(300*Widht),(50*Heigh)), nameStudentLine, customGuiStyle);
	GUI.Label(Rect((500*Widht),(470*Heigh),(300*Widht),(50*Heigh)), nameGroupLine, customGuiStyle);
    // If you do not want to apply the Style, do not provide the name
    //GUILayout.Button ("I am a normal UnityGUI Button without custom style -Русский текст без стиля-");
    
    
    GUI.Label(Rect((200*Widht),(700*Heigh),(600*Widht),(50*Heigh)), bottomLine, customGuiStyle);
    
}