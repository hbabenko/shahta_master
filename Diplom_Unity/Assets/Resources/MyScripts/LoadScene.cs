using UnityEngine;
using System.Collections;
using System.Linq;
using System.Xml;
using System.IO;


public class LoadScene : MonoBehaviour {
	
	public string localizedStringsFile;
	string BasicObjectPath = "Geometry/Basic/";
	string CustomObjectPath = "Geometry/Custom/";
	int ModuleSize = 4;
	int OneModuleBeginX;
	int OneModuleBeginZ;
	int OneModuleEndX;
	int OneModuleEndZ;
	// Use this for initialization
	void Start () {
	
		localizedStringsFile = "MyFile.xml";
	    XmlDocument Stage = new XmlDocument();
		Stage.Load(localizedStringsFile);
		string SomeData = Stage.SelectSingleNode("Scene/Module").InnerXml;
		
		XmlNodeList modules = Stage.GetElementsByTagName("Module");
		for (int i=0; i< modules.Count; i++)
		{
			ParceASimpleModule(modules[i]);
		}
	}
	
	void ParceASimpleModule(XmlNode module)
	{
		InitModule(module);
		//Parce Corner Object
		XmlNode _corner = module.SelectSingleNode("corner");
		GameObject Corner_grp = new GameObject();
		if(_corner!= null)
		{
			CreateCorner(_corner,Corner_grp);
		}
		//Parce main objects
		XmlNode _object = module.SelectSingleNode("objects");
		
		Debug.Log("Create Empty");
		//Its Parent Object For Module
		GameObject Unit_grp = new GameObject();
		
		AddABasicObject(_object,"wall",Unit_grp);
		AddABasicObject(_object,"floor",Unit_grp);
		AddABasicObject(_object,"crep",Unit_grp);
		AddABasicObject(_object,"top",Unit_grp);
		AddABasicObject(_object,"holder",Unit_grp);
		XmlNodeList custom_obj = _object.SelectNodes("custom");
		for (int i=0; i< custom_obj.Count; i++)
		{
			string custom_obj_name = custom_obj[i].InnerText;
			print("custom"+custom_obj_name);
			AddACustomObject(custom_obj_name,Unit_grp);
		}
		
		CreateKoridor(module, Unit_grp);
	
		
	}
	void InitModule(XmlNode module)
	{
		 OneModuleBeginX =int.Parse(module.SelectSingleNode("begin/Xd").InnerText);
		OneModuleBeginX = -OneModuleBeginX;
		 OneModuleBeginZ=int.Parse(module.SelectSingleNode("begin/Zd").InnerText);
		 OneModuleEndX =int.Parse(module.SelectSingleNode("end/Xd").InnerText);
		OneModuleEndX = -OneModuleEndX;
		 OneModuleEndZ =int.Parse(module.SelectSingleNode("end/Zd").InnerText);
	}
	void AddABasicObject(XmlNode _object ,string Name, GameObject Parent)
	{
		string new_object = _object.SelectSingleNode(Name).InnerText;
		print(new_object+" \n");
		GameObject _new_object = Instantiate(Resources.Load(BasicObjectPath +new_object)) as GameObject;
		if(_new_object!= null){
		_new_object.transform.parent = Parent.transform;
		}
	}
	void AddACustomObject(string Name, GameObject Parent)
	{
		if(Instantiate(Resources.Load(CustomObjectPath +Name))!= null){
			GameObject _new_object = Instantiate(Resources.Load(CustomObjectPath +Name)) as GameObject;
			_new_object.transform.parent = Parent.transform;
		}
	}
	void CreateCorner(XmlNode _corner, GameObject Corner_grp)
	{
		AddABasicObject(_corner,"wall",Corner_grp);
		AddABasicObject(_corner,"floor",Corner_grp);
		AddABasicObject(_corner,"top",Corner_grp);
		AddABasicObject(_corner,"holder",Corner_grp);
		
		int ModuleX = OneModuleBeginX*ModuleSize;
		int ModuleZ = OneModuleBeginZ*ModuleSize;
		
		Corner_grp.transform.Translate(ModuleX,0,ModuleZ);
		
		if ((OneModuleBeginZ==OneModuleEndZ)&&(OneModuleBeginX>OneModuleEndX))
			{//its East
				OneModuleBeginX = OneModuleBeginX - 1;
				
			}
			if ((OneModuleBeginZ==OneModuleEndZ)&&(OneModuleBeginX<OneModuleEndX))
			{//its East
				OneModuleBeginX = OneModuleBeginX + 1;
				
			}
			if ((OneModuleBeginZ>OneModuleEndZ)&&(OneModuleBeginX==OneModuleEndX))
			{//its North
				Corner_grp.transform.Rotate(0,-90,0);
			 	Corner_grp.transform.Translate(2,0,2);
				OneModuleBeginZ = OneModuleBeginZ - 1;
			}
			if ((OneModuleBeginZ<OneModuleEndZ)&&(OneModuleBeginX==OneModuleEndX))
			{//its South
				Corner_grp.transform.Rotate(0,90,0);
			 	Corner_grp.transform.Translate(2,0,2);
				OneModuleBeginZ = OneModuleBeginZ + 1;
				
			}
	}
	void CreateKoridor(XmlNode module, GameObject Module)
	{
		
		double Length = double.Parse(module.SelectSingleNode("objects/length").InnerText);
		print("Length "+Length+" \n");
		int ModuleX = OneModuleBeginX*ModuleSize;
		print("Module Start X "+ModuleX+" \n");
		int ModuleZ = OneModuleBeginZ*ModuleSize; 
		print("Module Start Z "+ModuleZ+" \n");
		for (int j=0; j<Length; j++)
		{
			GameObject Clone = Instantiate(Module) as GameObject;
			Clone.transform.Translate(ModuleX,0,ModuleZ);
			//Find The Direction
			if ((OneModuleBeginZ==OneModuleEndZ)&&(OneModuleBeginX>OneModuleEndX))
			{//its East
				ModuleX = ModuleX - ModuleSize;
				Clone.transform.Rotate(0,90,0);
				Clone.transform.Translate(2,0,2);
			}
			if ((OneModuleBeginZ==OneModuleEndZ)&&(OneModuleBeginX<OneModuleEndX))
			{//its East
				ModuleX = ModuleX + ModuleSize;
				Clone.transform.Rotate(0,-90,0);
				Clone.transform.Translate(2,0,2);
			}
			if ((OneModuleBeginZ>OneModuleEndZ)&&(OneModuleBeginX==OneModuleEndX))
			{//its North
				ModuleZ = ModuleZ - ModuleSize;
			}
			if ((OneModuleBeginZ<OneModuleEndZ)&&(OneModuleBeginX==OneModuleEndX))
			{//its South
				ModuleZ = ModuleZ + ModuleSize;
				
			}
		}
		
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
