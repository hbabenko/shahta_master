﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;


namespace Diplom_Creator
{
    public class CreateXML
    {
        //First - Create a Module object then - save it!

        static public void CreateFile()
        {
            //create temporary file
            XDocument tempFile = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                                                new XComment("Constructor file"),
                                                new XElement("Scene"));
            tempFile.Save("temp.xml");

        }

        static public XElement AddAHeader(Point_m StartPoint)
        {
            XElement Answer;
            //init start ppoint
            Answer = new XElement("header");
            Answer.Add(StartPoint.findXMLElement("start_point"));
            return Answer;
        }

        //Main Function
        static public void GenerateXMLFile(string FileName, Joint_m[] all_joints, Point_m StartPoint)
        {
            CreateFile();
            //Open document
            XDocument tempFile = XDocument.Load("temp.xml");

            tempFile.Element("Scene").Add(AddAHeader(StartPoint));

            foreach (Joint_m module in all_joints)
            {
                tempFile.Element("Scene").Add(module.findXMLElement());
            }
            tempFile.Save(FileName + ".xml");
        }
       
    }
}
