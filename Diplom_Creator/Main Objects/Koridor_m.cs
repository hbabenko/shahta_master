﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Diplom_Creator
{
    public class Koridor_m
    {
        //Объекты которые всегда будут в базе данных
        static string defaultWalls = "WallOut_cnr";
        static string defaultFloor = "Floor";
        static string defaultCrep = "";
        static string defaultTop = "Top_panel";
        static string defaultHolder = "WallIn_cnr";
        int Id;
        double Length;
        public string WallsName;
        public string FloorName;
        public string CrepName;
        public string TopName;
        public string HolderName;
        public List<string> AdditionalName;

        public Koridor_m(int _Id, double _Length)
        {
            Id = _Id;
            Length = _Length;
            AdditionalName = new List<string>();
        }
        public int Id_m()
        {
            return Id;
        }
        public double Length_m()
        {
            return Length;
        }
        public void setLength(double _length)
        {
            Length = _length;
        }
        public void MakeShorter()
        {
            Length--;
        }
        public XElement findXMLElement()
        {
            XElement Answer;
            Answer = new XElement("objects",
                        new XElement("length", Length),
                        new XElement("wall", WallsName),
                        new XElement("floor", FloorName),
                        new XElement("crep", CrepName),
                        new XElement("top", TopName),
                        new XElement("holder", HolderName));
            int i=1;
            foreach (string obj_name in AdditionalName)
            {
                XElement customObject = new XElement("custom", new XAttribute("id", i), obj_name);
                Answer.Add(customObject);
                i++;   
            }
            return Answer;
        }
        public string NameWithoutPrefix(string _name)
        {
            string[] splited = _name.Split('_');
            return splited[0];
        }
        public string FindCornerWalls()
        {
            string Answer;
            //Для начала надо найти такой же элемент в Базе но с cnr
            if (DataBase_UnityObject.hasObjectWithName(WallsName, "cnr"))
            { Answer = WallsName + "_cnr"; }
            else
            {   //стандартный объект
                Answer = defaultWalls;}
            return Answer;
        }
        public string FindCornerFloor()
        {
            string Answer;
            //Для начала надо найти такой же элемент в Базе но с cnr
            if (DataBase_UnityObject.hasObjectWithName(FloorName, "cnr"))
            { Answer = FloorName + "_cnr"; }
            else
            {   //стандартный объект
                Answer = defaultFloor;
            }
            return Answer;
 
        }
        public string FindCornerTop()
        {
            string Answer;
            //Для начала надо найти такой же элемент в Базе но с cnr
            if (DataBase_UnityObject.hasObjectWithName(TopName, "cnr"))
            { Answer = TopName + "_cnr"; }
            else
            {   //стандартный объект
                Answer = defaultTop;
            }
            return Answer;

        }
        public string FindCornerHolder()
        {
            string Answer;
            //Для начала надо найти такой же элемент в Базе но с cnr
            if (DataBase_UnityObject.hasObjectWithName(HolderName, "cnr"))
            { Answer = HolderName + "_cnr"; }
            else
            {   //стандартный объект
                Answer = defaultHolder;
            }
            return Answer;

        }
        public List<string> FindCornerAdditional()
        {
            List<string> CornerObject = new List<string>();
            foreach (string Add_obj in AdditionalName)
            {
                if (DataBase_UnityObject.hasObjectWithName(Add_obj, "cnr"))
                {
                    CornerObject.Add(Add_obj + "_cnr");
                }
            }
            return CornerObject;
        }
    }
}
