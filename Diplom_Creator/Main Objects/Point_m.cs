﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml.Linq;

namespace Diplom_Creator
{
    public class Point_m
    {
        static public int CurrentPointId;
        static public List<PointLimitition> CurrentPointLimit;
        //Настройка типов класса
        public enum PointType
        {
            aBegin_point = 0,
            aSimple_point = 1,
            aCross_point = 2,
            aLeftConor_point = 3,
            aRightConor_point = 4,
            aEnd_point = 5
        };
        //ограничение на возможность создать новую точку
        //aOnlyRightAngle прямой угол
        public enum PointLimitition
        {
            aNoLimit = 0,
            aOnlyRightAngle = 1,
            aNoNorth = 2,
            aNoSouth = 3,
            aNoEast = 4,
            aNoWest = 5
        };

        const int kCellHeight = 10;
        const int kCellWight = 10;
        const int kCenterOffcetX = 2;
        const int kCenterOffcetY = 2;
        const int kYCellCenter = 15 / 2;
        const int kXCellCenter = 15 / 2;
        //свойства
        private int X;
        private int Y;

        private int localX;
        private int localY;

        private PointType type;
        private int Id;

        //конструктор для начальной точки
        public Point_m(int _X, int _Y)
        {
            X = _X;
            Y=_Y;
            Id = GenerateId();
            type = PointType.aBegin_point;
            localX = 0;
            localY = 0;
        }
        //конструктор для последующих точек
        public Point_m(int _X, int _Y, Point_m StartPoint,PointType _type)
        {
            X = _X;
            Y = _Y;
            type = _type;
            Id = GenerateId();
            localX = _X - StartPoint.X_m();
            localY = _Y - StartPoint.Y_m();
        }
        private int GenerateId()
        {
            //пока что просто добавляем значение
            CurrentPointId++;
            return CurrentPointId;
        }
        //Интерфейс для отображения скрытых параметров
        public int X_m()
        {
            return X;
        }
        public int Y_m()
        {
            return Y;
        }
        public PointType pointType()
        {
            return type;
        }
        public int Id_m()
        {
            return Id;
        }

        //Переводим точку в XML документ
        public XElement findXMLElement(string Name)
        {
            XElement Answer;
            Answer = new XElement(Name,
                        new XElement("Xd", localX),
                        new XElement("Zd", localY));
                       
            return Answer;
        }
        public Point _Point()
        {
            Point newPoint = new Point(Point_m.XtoCellCenterPixel(X), Point_m.YtoCellCenterPixel(Y));
            return newPoint;
        }
        public static double findDistance(Point_m First, Point_m Next)
        {
            int dX = (Next.X_m() - First.X_m()) * (Next.X_m() - First.X_m());
            int dY = (Next.Y_m() - First.Y_m()) * (Next.Y_m() - First.Y_m());
            double distance = Math.Sqrt(dX + dY);
            return distance;
 
        }
        public static int XtoCellCenterPixel(int _X)
        {
            return (Point_m.XtoPixel(_X) + kXCellCenter);
        }
        public static int YtoCellCenterPixel(int _Y)
        {
            return (Point_m.YtoPixel(_Y) + kYCellCenter);
        }
        public static int XtoPixel(int _X)
        {
            return (_X*kCellWight + kCenterOffcetX);
        }
        public static int YtoPixel(int _Y)
        {
            return (_Y*kCellHeight + kCenterOffcetY);
        }
        public static int PixeltoX(int _X)
        {
            return (int)(_X / kCellWight);
        }
        public static int PixeltoY(int _Y)
        {
            return (int)(_Y / kCellHeight);
        }

        //статические методі для упрощения работі с точками
        public static bool canWeCreatePoint(int cX, int cY, int CoordX, int CoordY)
        {
            //В этой точке мы уже должны знать направление
            //и ограничения
            //Задаются при создании точки


            //Проверяем остальные ограничения
            bool sameX = false;
            bool sameY = false;
            if (CoordX == cX) { sameX = true; }
            if (CoordY == cY) { sameY = true; }

            //Мы выбрали ту же точку
            if ((sameX == true) && (sameY == true)) { return false; }

            //Проверяем угол
            if (CurrentPointLimit.Contains(PointLimitition.aOnlyRightAngle))
            {
                if ((sameX == false) && (sameY == false))
                {
                    //не прямой угол
                    return false;
                }
            }
            if (CurrentPointLimit.Contains(PointLimitition.aNoLimit))
            { return true; }
            //Проверяем стороны
            if (CurrentPointLimit.Contains(PointLimitition.aNoEast))
            {
                if (sameY && (CoordX < cX))
                    return false;
            }
            if (CurrentPointLimit.Contains(PointLimitition.aNoNorth))
            {
                if (sameX && (CoordY > cY))
                    return false;
            }
            if (CurrentPointLimit.Contains(PointLimitition.aNoSouth))
            {
                if (sameX && (CoordY < cY))
                    return false;
            }
            if (CurrentPointLimit.Contains(PointLimitition.aNoWest))
            {
                if (sameY && (CoordX > cX))
                    return false;
            }
            return true;
        }

        public static void setStandartPointLimitation(PointType newPointType, Joint_m.BuildDirection CurrentDirection)
        {
            //wa already set direction
            //clear previous limitation
            CurrentPointLimit.Clear();
            switch (newPointType)
            {
                case PointType.aBegin_point:
                    {
                        //No Limitation
                        CurrentPointLimit.Add(PointLimitition.aNoLimit);
                        CurrentPointLimit.Add(PointLimitition.aOnlyRightAngle);
                        CurrentPointLimit.Add(cantGoBack(CurrentDirection));
                        CurrentPointLimit.Add(cantGoLeft(CurrentDirection));
                        CurrentPointLimit.Add(cantGoRight(CurrentDirection));
                        break;
                    }
                case PointType.aSimple_point:
                case PointType.aCross_point:
                case PointType.aLeftConor_point:
                case PointType.aRightConor_point:
                    {
                        CurrentPointLimit.Add(PointLimitition.aOnlyRightAngle);
                        CurrentPointLimit.Add(cantGoBack(CurrentDirection));
                        CurrentPointLimit.Add(cantGoLeft(CurrentDirection));
                        CurrentPointLimit.Add(cantGoRight(CurrentDirection));
                        break;
                    }
                case PointType.aEnd_point:
                    {
                        //No Limitation
                        CurrentPointLimit.Add(PointLimitition.aOnlyRightAngle);
                        CurrentPointLimit.Add(PointLimitition.aNoEast);
                        CurrentPointLimit.Add(PointLimitition.aNoNorth);
                        CurrentPointLimit.Add(PointLimitition.aNoSouth);
                        CurrentPointLimit.Add(PointLimitition.aNoWest);
                        break;
                    }
                default:
                    {
                        //No Limitation
                        CurrentPointLimit.Add(PointLimitition.aNoLimit);
                        break;
                    }
            }
        }
       

        //find point limitations
        static private PointLimitition cantGoStraight(Joint_m.BuildDirection CurrentDirection)
        {
            switch (CurrentDirection)
            {
                case Joint_m.BuildDirection.aNorth:
                    return PointLimitition.aNoNorth;
                case Joint_m.BuildDirection.aEast:
                    return PointLimitition.aNoEast;
                case Joint_m.BuildDirection.aSouth:
                    return PointLimitition.aNoSouth;
                case Joint_m.BuildDirection.aWest:
                    return PointLimitition.aNoWest;
            }
            return PointLimitition.aNoNorth;
        }
        static private PointLimitition cantGoBack(Joint_m.BuildDirection CurrentDirection)
        {
            switch (CurrentDirection)
            {
                case Joint_m.BuildDirection.aNorth:
                    return PointLimitition.aNoSouth;
                case Joint_m.BuildDirection.aEast:
                    return PointLimitition.aNoWest;
                case Joint_m.BuildDirection.aSouth:
                    return PointLimitition.aNoNorth;
                case Joint_m.BuildDirection.aWest:
                    return PointLimitition.aNoEast;
            }
            return PointLimitition.aNoSouth;
        }
        static private PointLimitition cantGoLeft(Joint_m.BuildDirection CurrentDirection)
        {
            switch (CurrentDirection)
            {
                case Joint_m.BuildDirection.aNorth:
                    return PointLimitition.aNoWest;
                case Joint_m.BuildDirection.aEast:
                    return PointLimitition.aNoNorth;
                case Joint_m.BuildDirection.aSouth:
                    return PointLimitition.aNoEast;
                case Joint_m.BuildDirection.aWest:
                    return PointLimitition.aNoSouth;
            }
            return PointLimitition.aNoWest;
        }
        static private PointLimitition cantGoRight(Joint_m.BuildDirection CurrentDirection)
        {
            switch (CurrentDirection)
            {
                case Joint_m.BuildDirection.aNorth:
                    return PointLimitition.aNoEast;
                case Joint_m.BuildDirection.aEast:
                    return PointLimitition.aNoSouth;
                case Joint_m.BuildDirection.aSouth:
                    return PointLimitition.aNoWest;
                case Joint_m.BuildDirection.aWest:
                    return PointLimitition.aNoNorth;
            }
            return PointLimitition.aNoEast;
        }
        static public Image PointImage(Point_m.PointType PointSelected)
        {
            switch (PointSelected)
            {
                case Point_m.PointType.aBegin_point:
                    return Diplom_Creator.Properties.Resources.point_beginS;
                case Point_m.PointType.aSimple_point:
                    return Diplom_Creator.Properties.Resources.point_simpleS;
                case Point_m.PointType.aCross_point:
                    return Diplom_Creator.Properties.Resources.point_crossS;
                case Point_m.PointType.aLeftConor_point:
                case Point_m.PointType.aRightConor_point:
                    return Diplom_Creator.Properties.Resources.point_conorS;
                case Point_m.PointType.aEnd_point:
                    return Diplom_Creator.Properties.Resources.point_endS;
                default:
                    return Diplom_Creator.Properties.Resources.point_endS;
            }

        }
    }
}
