﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Diplom_Creator.Main_Objects
{
    class Corner_m
    {
        //Класс, для автоматического создания угла для поворота или перекрестка
        static List<Corner_m> ListOfCrosses;

        int kMinWays = 2;
        int kMaxWays = 4;
        public enum CrossType
        {
            aStraight = 2,
            aTreeWaysLeft = 3,
            aTreeWaysRight = 4,
            aFourWays = 5,
        };
        private int _Point_id;
        private int _CrossType;

        public int Point_id()
        {
            return _Point_id;
        }
        public int Cross_Type()
        {
            return _CrossType;
        }
        public Corner_m(int _point_id, int _cross_type)
        {
            _Point_id = _point_id;
            if ((_cross_type >= kMinWays) && (_cross_type <= kMaxWays))
            {
                _CrossType = _cross_type;
            }
        }

        static public void setListOfCrosses(List<Point_m> all_points)
        {
            if (ListOfCrosses == null)
                ListOfCrosses = new List<Corner_m>();
            ListOfCrosses.Clear();
            foreach (Point_m _point in all_points)
            {
                Console.WriteLine("NEW POINT"+ _point.Id_m()+" X" + _point.X_m()+"Y" + _point.Y_m());
            }
            var groupPoints = all_points.GroupBy(x => x.Id_m());
            foreach (var group in groupPoints)
            {
                Point_m[] _points = group.ToArray();
                if (_points[0].pointType() == Point_m.PointType.aCross_point)
                {
                    
                    int _CrossType = 1;
                    if (_points.Count() == 1)
                        _CrossType = (int)CrossType.aStraight;
                    if (_points.Count() == 2)
#warning CANT SET a LEFT CROSS!!!
                        _CrossType = (int)CrossType.aTreeWaysRight;
                    if (_points.Count() == 3)
                        _CrossType = (int)CrossType.aFourWays;
                    ListOfCrosses.Add(new Corner_m(_points[0].Id_m(), _CrossType));
                }
            }
            
        }
       
        static public XElement addLeftCornerToKoridor(Koridor_m _koridor)
        {
            XElement Answer = new XElement("corner");
            Answer = createTreeWayLeftCross(_koridor);
            Answer.Element("corner").Add(new XElement("end_top", "WallDeadEnd"));
            return Answer;
        }
        static public XElement addRightCornerToKoridor(Koridor_m _koridor)
        {
            XElement Answer = new XElement("corner");
            Answer = createTreeWayRightCross(_koridor);
            Answer.Element("corner").Add(new XElement("end_top", "WallDeadEnd"));
            return Answer;
        }
        static public XElement addСrossToKoridor(Koridor_m _koridor, int Point_Id)
        {
            XElement Answer = new XElement("corner");
            Corner_m _coridor = ListOfCrosses.Find(x => x.Point_id() == Point_Id);
            switch (_coridor._CrossType)
            {
                case (int)CrossType.aStraight:
                    {
                        Answer = createStraightWayCross(_koridor);
                        break; }
                case (int)CrossType.aTreeWaysLeft:
                    {
                        Answer = createTreeWayLeftCross(_koridor);
                        break; }
                case (int)CrossType.aTreeWaysRight:
                    {
                        Answer = createTreeWayRightCross(_koridor);
                        break;
                    }
                case (int)CrossType.aFourWays:
                    {
                        Answer = createFourWayCross(_koridor);
                        break; }
            }

            return Answer;
        }
        static public XElement createTreeWayRightCross(Koridor_m _koridor)
        {
            //Все обьекты пока есть только в правом повороте...
            //Остальные составляются из пола и верхней панели....
            XElement Answer;
            Answer = new XElement("corner",
                        new XElement("wall", _koridor.FindCornerWalls()),
                        new XElement("floor", _koridor.FindCornerFloor()),
                        new XElement("top", _koridor.FindCornerTop()),
                        new XElement("holder", _koridor.FindCornerHolder()));
            int i = 1;
            foreach (string obj_name in _koridor.FindCornerAdditional())
            {
                XElement customObject = new XElement("custom", new XAttribute("id", i), obj_name);
                Answer.Add(customObject);
                i++;
            }
            return Answer;
        }
        static public XElement createStraightWayCross(Koridor_m _koridor)
        {
            //Чтобы не ломать существующий алгоритм для ровного поворота просто копируем первый модуль
            XElement Answer;
            Answer = new XElement("corner",
                        new XElement("wall", _koridor.WallsName),
                        new XElement("floor", _koridor.FloorName),
                        new XElement("crep", _koridor.CrepName),
                        new XElement("top", _koridor.TopName),
                        new XElement("holder", _koridor.FindCornerHolder()));
            int i = 1;
            foreach (string obj_name in _koridor.FindCornerAdditional())
            {
                XElement customObject = new XElement("custom", new XAttribute("id", i), obj_name);
                Answer.Add(customObject);
                i++;
            }
            return Answer;
        }
        static public XElement createTreeWayLeftCross(Koridor_m _koridor)
        {
            XElement Answer;
            Answer = new XElement("corner",
                        new XElement("floor", _koridor.FindCornerFloor()),
                        new XElement("top", _koridor.FindCornerTop()),
                        new XElement("end_right", "WallDeadEnd"));
            /*int i = 1;
            foreach (string obj_name in _koridor.FindCornerAdditional())
            {
                XElement customObject = new XElement("custom", new XAttribute("id", i), obj_name);
                Answer.Add(customObject);
                i++;
            }*/
            return Answer;
        }
        static public XElement createFourWayCross(Koridor_m _koridor)
        {
            XElement Answer;
            Answer = new XElement("corner",
                        new XElement("floor", _koridor.FindCornerFloor()),
                        new XElement("top", _koridor.FindCornerTop()));
            /*int i = 1;
            foreach (string obj_name in _koridor.FindCornerAdditional())
            {
                XElement customObject = new XElement("custom", new XAttribute("id", i), obj_name);
                Answer.Add(customObject);
                i++;
            }*/
            return Answer;
        }
        
    }
}
