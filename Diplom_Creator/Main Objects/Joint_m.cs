﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Diplom_Creator
{
    public class Joint_m
    {
        static public int CurrentJointId;
        public enum BuildDirection
        {
            aNorth = 0,
            aSouth = 1,
            aEast = 2,
            aWest = 3
        }
        //состоит из коридора и двух точек между ним
        public Koridor_m Koridor;
        public Point_m EndPoint;
        public Point_m BeginPoint;
       
        int Id;

        public Joint_m(Koridor_m _koridor, Point_m  _beginPoint, Point_m _endPoint)
        {

            Id = this.GenerateJointId();
            EndPoint = new Point_m(0, 0);
            Koridor = new Koridor_m(0, 0);
            BeginPoint = new Point_m(0, 0);
            EndPoint = _endPoint;
            BeginPoint = _beginPoint;
            Koridor = _koridor;
        }
        private int GenerateJointId()
        {
            return CurrentJointId++;
        }
        public BuildDirection findJointDirection()
        {
            BuildDirection by_default = BuildDirection.aNorth;
            bool sameX = false;
            bool sameY = false;

            //сократим обозначения
            int X1 = BeginPoint.X_m();
            int X2 = EndPoint.X_m();
            int Y1 = BeginPoint.Y_m();
            int Y2 = EndPoint.Y_m();

            if (X1 == X2) { sameX = true; }
            if (Y1 == Y2) { sameY = true; }

            //Вообще-то єто- ошибка, если все работает правильно такого быть не может
            if ((sameX == true) && (sameY == true)) {
                throw new System.ArgumentException("Point Start and End is the same!");
            }
            if ((sameX == false) && (sameY == false))
            {
                throw new System.ArgumentException("Strange Direction!");
            }
           
            //Пока что доступны только 4 направления:
            if (sameY && (X1 < X2))
                    return BuildDirection.aEast;
       
            if (sameX && (Y1 > Y2))
                    return BuildDirection.aNorth;

            if (sameX && (Y1 < Y2))
                return BuildDirection.aSouth;

            if (sameY && (X1 > X2))
                return BuildDirection.aWest;

            //пока что возвращаем значение по умолчанию
            return by_default;
           
        }
        public int Koridor_id_m()
        {
            return Koridor.Id_m();
        }
        public int EndPoint_id_m()
        {
            return EndPoint.Id_m();
        }
        public int BeginPoint_id_m()
        {
            return BeginPoint.Id_m();
        }
        public int Id_m()
        {
            return Id;
        }
        //Перед тем как генирировать файл нужно проверить,  нет ли каких либо ошибок в схеме
        /*static public bool canWeGenerateFileWithWay(List<Joint_m> all_way)
        {
            //первая проверка - есть ли не закрытые перекрестки

            //вторая проверка - не пересекается ли коридор друг с другом(ее пока нет)
        }*/

        public XElement findXMLElement()
        {
             XElement Answer;
             Answer = new XElement("Module", new XAttribute("id", Id));
             Answer.Add(BeginPoint.findXMLElement("begin"));

             Console.WriteLine("Koridor LENGTH"+Koridor.Length_m());
             if (BeginPoint.pointType() == Point_m.PointType.aCross_point)
             {
                 Answer.Add(Main_Objects.Corner_m.addСrossToKoridor(Koridor, BeginPoint.Id_m()));
                 //Сокращаем коридор поскольку один єлемент уже занимает угол
                 Koridor.MakeShorter();
             }
             if (BeginPoint.pointType() == Point_m.PointType.aLeftConor_point)
             {
                 Answer.Add(Main_Objects.Corner_m.addLeftCornerToKoridor(Koridor));
                 //Сокращаем коридор поскольку один єлемент уже занимает угол
                 Koridor.MakeShorter();
             }
             if (BeginPoint.pointType() == Point_m.PointType.aRightConor_point)
             {
                 Answer.Add(Main_Objects.Corner_m.addRightCornerToKoridor(Koridor));
                 //Сокращаем коридор поскольку один єлемент уже занимает угол
                 Koridor.MakeShorter();
             }
             Console.WriteLine("Koridor LENGTH FINALY" + Koridor.Length_m());
             Answer.Add(Koridor.findXMLElement());
             Answer.Add(EndPoint.findXMLElement("end"));
             return Answer;

        }
        //Поиск направлений
        static public List<BuildDirection> findClosedDirectionsById(List<Joint_m> all_way, int Point_Id)
        {

            List<BuildDirection> ClosedDirections = new List<BuildDirection>();
            int currentJoint =0;
            //Ищем среди всех путей которые мы уже добавили
            foreach (Joint_m oneJoint in all_way)
            {
                //Перекресток может быть только раз конечной точкой 
                // и 3 раза конечной начальной
                if (oneJoint.BeginPoint.Id_m() == Point_Id)
                {
                    ClosedDirections.Add(oneJoint.findJointDirection());
                }
                else
                {
                    if (oneJoint.EndPoint.Id_m() == Point_Id)
                    {
                        BuildDirection currentDirection = oneJoint.findJointDirection();
                        ClosedDirections.Add(Joint_m.findOppositeDirection(currentDirection));
                    }
                }
                currentJoint ++;

            }
            return ClosedDirections;
        }
        static public List<BuildDirection> onlyStraightDirection(BuildDirection _current)
        {
            List<BuildDirection> ClosedDirections = new List<BuildDirection>();
            ClosedDirections.Add(BuildDirection.aEast);
            ClosedDirections.Add(BuildDirection.aNorth);
            ClosedDirections.Add(BuildDirection.aSouth);
            ClosedDirections.Add(BuildDirection.aWest);
            ClosedDirections.Remove(_current);
            return ClosedDirections;
        }
        static public BuildDirection findOppositeDirection(BuildDirection _direction)
        {
            switch (_direction)
            {
                case BuildDirection.aNorth:
                    return BuildDirection.aSouth;
                case BuildDirection.aSouth:
                    return BuildDirection.aNorth;
                case BuildDirection.aEast:
                    return BuildDirection.aWest;
                case BuildDirection.aWest:
                    return BuildDirection.aEast;
            }
            return BuildDirection.aNorth;
        }
    }
}
