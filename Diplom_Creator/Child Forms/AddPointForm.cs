﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{
    public partial class AddPointForm : Form
    {
        //В этом классе никаких вычислений, просто запускаем соответсующие классы и функции
       
        private Point_m.PointType ChosenPoint;

        public delegate void AddPointHandler(Point_m.PointType ChosenPointType);

        //Events
        public static event AddPointHandler AddPoint;

        public AddPointForm()
        {
            ChosenPoint = 0;
            InitializeComponent();
        }
        public void createFirstPoint()
        {
            point_begin.Enabled = true;
            point_simple.Enabled = false;
            point_cross.Enabled = false;
            point_left_conor.Enabled = false;
            point_right_conor.Enabled = false;
            point_end.Enabled = false;
        }
        public void createNextPoints()
        {
            point_begin.Enabled = false;
            point_simple.Enabled = true;
            point_cross.Enabled = true;
            point_left_conor.Enabled = true;
            point_right_conor.Enabled = true;
            point_end.Enabled = true;
 
        }
        private void complete_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Create button complete");
            //добавить точку
            AddPoint(ChosenPoint);
            //скрываем окно
            this.Hide();
        }

        private void point_begin_CheckedChanged(object sender, EventArgs e)
        {
            if (point_begin.Checked)
            {
                ChosenPoint = Point_m.PointType.aBegin_point;
                this.type_picture.BackgroundImage = Diplom_Creator.Properties.Resources.point_begin;
            }
        }

        private void point_simple_CheckedChanged(object sender, EventArgs e)
        {
            if (point_simple.Checked)
            {
                ChosenPoint = Point_m.PointType.aSimple_point;
                this.type_picture.BackgroundImage = Diplom_Creator.Properties.Resources.point_simple;
            }

        }

        private void point_cross_CheckedChanged(object sender, EventArgs e)
        {
            if (point_cross.Checked)
            {
                ChosenPoint = Point_m.PointType.aCross_point;
                this.type_picture.BackgroundImage = Diplom_Creator.Properties.Resources.point_cross;
            }
        }
        private void point_end_CheckedChanged(object sender, EventArgs e)
        {
            if (point_end.Checked)
            {
                ChosenPoint = Point_m.PointType.aEnd_point;
                this.type_picture.BackgroundImage = Diplom_Creator.Properties.Resources.point_end;
            }
        }

        private void point_right_conor_CheckedChanged(object sender, EventArgs e)
        {
            if (point_right_conor.Checked)
            {
                ChosenPoint = Point_m.PointType.aRightConor_point;
                this.type_picture.BackgroundImage = Diplom_Creator.Properties.Resources.point_conor;
            }
        }

        private void point_left_conor_CheckedChanged(object sender, EventArgs e)
        {
            if (point_left_conor.Checked)
            {
                ChosenPoint = Point_m.PointType.aLeftConor_point;
                this.type_picture.BackgroundImage = Diplom_Creator.Properties.Resources.point_conor;
            }

        }
    }
}
