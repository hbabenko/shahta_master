﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{
    public partial class AddCoridorForm : Form
    {
        
        public delegate void AddKoridorHandler(Diplom_Creator.Koridor_m _Koridor);

        //Events
        public static event AddKoridorHandler AddKoridor;
        UnityObject.ObjectType SelectedField;

        public Diplom_Creator.Koridor_m newKoridor;

        public AddCoridorForm()
        {
            InitializeComponent();
            if (newKoridor == null)
             newKoridor = new Koridor_m(-1,0);
            InitAll_list();
        }
        private void InitChoose_listWithType(UnityObject.ObjectType _type)
        {
            List<UnityObject> _Objects = DataBase_UnityObject.GetObjectWithType(_type);
            this.choose_list.Items.Clear();
            foreach (UnityObject _One in _Objects)
            {
                //Угловые обьекты нельзя начначать для постройки коридора!
                //Они нужны для того, чтобы соединить 2 коридора
                if(!Equals(_One.Prefix(),"cnr")){
                    if (!Equals(_One.Prefix(), "prb"))
                    {
                        this.choose_list.Items.Add(_One.FullName());
                    }
                }
            }
        }

        private void InitAdditional_list()
        {
            
        }

        private void InitAll_list()
        {
            List<UnityObject> _Objects = DataBase_UnityObject.GetObjectWithType(UnityObject.ObjectType.Custom);
            _Objects.AddRange(DataBase_UnityObject.GetObjectWithType(UnityObject.ObjectType.Animation));
            this.all_list.Items.Clear();
            foreach (UnityObject _One in _Objects)
            {
                if (!Equals(_One.Prefix(), "cnr"))
                {
                    if (!Equals(_One.Prefix(), "prb"))
                    {
                        this.all_list.Items.Add(_One.FullName());
                    }
                }
            }
 
        }
        private void walls_btn_Click(object sender, EventArgs e)
        {
            SelectedField = UnityObject.ObjectType.Walls;
            InitChoose_listWithType(UnityObject.ObjectType.Walls);
        }

        private void crep_btn_Click(object sender, EventArgs e)
        {
            SelectedField = UnityObject.ObjectType.Crep;
            InitChoose_listWithType(UnityObject.ObjectType.Crep);
        }

        private void floor_btn_Click(object sender, EventArgs e)
        {
            SelectedField = UnityObject.ObjectType.Floor;
            InitChoose_listWithType(UnityObject.ObjectType.Floor);
        }

        private void top_btn_Click(object sender, EventArgs e)
        {
            SelectedField = UnityObject.ObjectType.Top;
            InitChoose_listWithType(UnityObject.ObjectType.Top);
        }

        private void holder_btn_Click(object sender, EventArgs e)
        {
            SelectedField = UnityObject.ObjectType.Holder;
            InitChoose_listWithType(UnityObject.ObjectType.Holder);
        }

        private bool CheckAllValid()
        {
            if (string.Equals(walls_txt.Text, "Выбрать"))
            {
                MessageBox.Show("Выберите стены!");
                return false;
            }
            else
            {
                newKoridor.WallsName = walls_txt.Text;
            }
            if (string.Equals(floor_txt.Text, "Выбрать"))
            {
                MessageBox.Show("Выберите пол!");
                return false;
            }
            else
            {
                newKoridor.FloorName = floor_txt.Text;
            }
            if (string.Equals(crep_txt.Text, "Выбрать"))
            {
                MessageBox.Show("Выберите крепь!");
                return false;
            }
            else
            {
                newKoridor.CrepName = crep_txt.Text;
            }
            if (string.Equals(top_txt.Text , "Выбрать"))
            {
                MessageBox.Show("Выберите потолок!");
                return false;
            }
            else
            {
                newKoridor.TopName = top_txt.Text;
            }
            if (string.Equals(holder_txt.Text, "Выбрать"))
            {
                MessageBox.Show("Выберите держатель!");
                return false;
            }
            else
            {
                newKoridor.HolderName = holder_txt.Text;
            }
            return true;
        }
        private void saveCustomObjects()
        {
            newKoridor.AdditionalName.Clear();
            foreach (object custom in additional_list.Items)
            {
                newKoridor.AdditionalName.Add("" + custom);
            }
        }
        private void save_btn_Click(object sender, EventArgs e)
        {
            if (CheckAllValid())
            {
                saveCustomObjects();
                AddKoridor(newKoridor);
                this.Hide();
            }
        }

        private void choose_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (SelectedField)
            {
                case UnityObject.ObjectType.Walls:
                    {
                        walls_txt.Text = "" + this.choose_list.SelectedItem;
                        break;
                    }
                case UnityObject.ObjectType.Floor:
                    {
                        floor_txt.Text = "" + this.choose_list.SelectedItem; 
                        break;
                    }
                case UnityObject.ObjectType.Crep:
                    {
                        crep_txt.Text = "" + this.choose_list.SelectedItem;
                        break;
                    }
                case UnityObject.ObjectType.Top:
                    {
                        top_txt.Text = "" + this.choose_list.SelectedItem;
                        break;
                    }
                case UnityObject.ObjectType.Holder:
                    {
                        holder_txt.Text = "" + this.choose_list.SelectedItem;
                        break;
                    }
                default:
                    break;
            }
            
        }
        private void additional_list_DoubleClick(object sender, EventArgs e)
        {
            this.additional_list.Items.RemoveAt(additional_list.SelectedIndex);
        }

        private void all_list_DoubleClick(object sender, EventArgs e)
        {
            this.additional_list.Items.Add(all_list.SelectedItem);

        }

        private void remove_btn_Click(object sender, EventArgs e)
        {
            if (additional_list.SelectedIndex >= 0)
            {
                this.all_list.Items.Add(additional_list.SelectedItem);
                this.additional_list.Items.RemoveAt(additional_list.SelectedIndex);

            }
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (all_list.SelectedIndex >= 0)
            {
                this.additional_list.Items.Add(all_list.SelectedItem);
                this.all_list.Items.RemoveAt(all_list.SelectedIndex);
            }
        }


    }
}
