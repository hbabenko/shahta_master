﻿namespace Diplom_Creator
{
    partial class AddPointForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.point_begin = new System.Windows.Forms.RadioButton();
            this.point_simple = new System.Windows.Forms.RadioButton();
            this.point_cross = new System.Windows.Forms.RadioButton();
            this.point_left_conor = new System.Windows.Forms.RadioButton();
            this.point_end = new System.Windows.Forms.RadioButton();
            this.complete = new System.Windows.Forms.Button();
            this.type_picture = new System.Windows.Forms.PictureBox();
            this.point_right_conor = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.type_picture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ТИП:";
            // 
            // point_begin
            // 
            this.point_begin.AutoSize = true;
            this.point_begin.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_begin.Location = new System.Drawing.Point(112, 23);
            this.point_begin.Name = "point_begin";
            this.point_begin.Size = new System.Drawing.Size(128, 21);
            this.point_begin.TabIndex = 3;
            this.point_begin.TabStop = true;
            this.point_begin.Text = "Начальная точка";
            this.point_begin.UseVisualStyleBackColor = true;
            this.point_begin.CheckedChanged += new System.EventHandler(this.point_begin_CheckedChanged);
            // 
            // point_simple
            // 
            this.point_simple.AutoSize = true;
            this.point_simple.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_simple.Location = new System.Drawing.Point(112, 50);
            this.point_simple.Name = "point_simple";
            this.point_simple.Size = new System.Drawing.Size(118, 21);
            this.point_simple.TabIndex = 4;
            this.point_simple.TabStop = true;
            this.point_simple.Text = "Обычная точка";
            this.point_simple.UseVisualStyleBackColor = true;
            this.point_simple.CheckedChanged += new System.EventHandler(this.point_simple_CheckedChanged);
            // 
            // point_cross
            // 
            this.point_cross.AutoSize = true;
            this.point_cross.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_cross.Location = new System.Drawing.Point(112, 77);
            this.point_cross.Name = "point_cross";
            this.point_cross.Size = new System.Drawing.Size(103, 21);
            this.point_cross.TabIndex = 5;
            this.point_cross.TabStop = true;
            this.point_cross.Text = "Перекресток";
            this.point_cross.UseVisualStyleBackColor = true;
            this.point_cross.CheckedChanged += new System.EventHandler(this.point_cross_CheckedChanged);
            // 
            // point_left_conor
            // 
            this.point_left_conor.AutoSize = true;
            this.point_left_conor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_left_conor.Location = new System.Drawing.Point(112, 104);
            this.point_left_conor.Name = "point_left_conor";
            this.point_left_conor.Size = new System.Drawing.Size(120, 21);
            this.point_left_conor.TabIndex = 6;
            this.point_left_conor.TabStop = true;
            this.point_left_conor.Text = "Левый поворот";
            this.point_left_conor.UseVisualStyleBackColor = true;
            this.point_left_conor.CheckedChanged += new System.EventHandler(this.point_left_conor_CheckedChanged);
            // 
            // point_end
            // 
            this.point_end.AutoSize = true;
            this.point_end.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_end.Location = new System.Drawing.Point(112, 156);
            this.point_end.Name = "point_end";
            this.point_end.Size = new System.Drawing.Size(59, 21);
            this.point_end.TabIndex = 7;
            this.point_end.TabStop = true;
            this.point_end.Text = "Тупик";
            this.point_end.UseVisualStyleBackColor = true;
            this.point_end.CheckedChanged += new System.EventHandler(this.point_end_CheckedChanged);
            // 
            // complete
            // 
            this.complete.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.complete.Location = new System.Drawing.Point(65, 183);
            this.complete.Name = "complete";
            this.complete.Size = new System.Drawing.Size(140, 35);
            this.complete.TabIndex = 8;
            this.complete.Text = "ДОБАВИТЬ";
            this.complete.UseVisualStyleBackColor = true;
            this.complete.Click += new System.EventHandler(this.complete_Click);
            // 
            // type_picture
            // 
            this.type_picture.BackgroundImage = global::Diplom_Creator.Properties.Resources.point_begin;
            this.type_picture.Location = new System.Drawing.Point(15, 67);
            this.type_picture.Name = "type_picture";
            this.type_picture.Size = new System.Drawing.Size(65, 65);
            this.type_picture.TabIndex = 2;
            this.type_picture.TabStop = false;
            // 
            // point_right_conor
            // 
            this.point_right_conor.AutoSize = true;
            this.point_right_conor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_right_conor.Location = new System.Drawing.Point(112, 129);
            this.point_right_conor.Name = "point_right_conor";
            this.point_right_conor.Size = new System.Drawing.Size(128, 21);
            this.point_right_conor.TabIndex = 9;
            this.point_right_conor.TabStop = true;
            this.point_right_conor.Text = "Правый поворот";
            this.point_right_conor.UseVisualStyleBackColor = true;
            this.point_right_conor.CheckedChanged += new System.EventHandler(this.point_right_conor_CheckedChanged);
            // 
            // AddPointForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(266, 230);
            this.ControlBox = false;
            this.Controls.Add(this.point_right_conor);
            this.Controls.Add(this.complete);
            this.Controls.Add(this.point_end);
            this.Controls.Add(this.point_left_conor);
            this.Controls.Add(this.point_cross);
            this.Controls.Add(this.point_simple);
            this.Controls.Add(this.point_begin);
            this.Controls.Add(this.type_picture);
            this.Controls.Add(this.label1);
            this.Name = "AddPointForm";
            this.Text = "ДОБАВИТЬ ТОЧКУ";
            ((System.ComponentModel.ISupportInitialize)(this.type_picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox type_picture;
        private System.Windows.Forms.RadioButton point_begin;
        private System.Windows.Forms.RadioButton point_simple;
        private System.Windows.Forms.RadioButton point_cross;
        private System.Windows.Forms.RadioButton point_left_conor;
        private System.Windows.Forms.RadioButton point_end;
        private System.Windows.Forms.Button complete;
        private System.Windows.Forms.RadioButton point_right_conor;
    }
}