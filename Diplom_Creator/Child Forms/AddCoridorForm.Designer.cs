﻿namespace Diplom_Creator
{
    partial class AddCoridorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.settings = new System.Windows.Forms.TabControl();
            this.standart = new System.Windows.Forms.TabPage();
            this.standart_img = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.holder_btn = new System.Windows.Forms.Button();
            this.holder_txt = new System.Windows.Forms.Label();
            this.holder_lbl = new System.Windows.Forms.Label();
            this.top_btn = new System.Windows.Forms.Button();
            this.top_txt = new System.Windows.Forms.Label();
            this.top_lbl = new System.Windows.Forms.Label();
            this.crep_btn = new System.Windows.Forms.Button();
            this.crep_txt = new System.Windows.Forms.Label();
            this.crep_lbl = new System.Windows.Forms.Label();
            this.floor_btn = new System.Windows.Forms.Button();
            this.floor_txt = new System.Windows.Forms.Label();
            this.floor_lbl = new System.Windows.Forms.Label();
            this.choose_list = new System.Windows.Forms.ListBox();
            this.walls_btn = new System.Windows.Forms.Button();
            this.walls_txt = new System.Windows.Forms.Label();
            this.walls_lbl = new System.Windows.Forms.Label();
            this.additional = new System.Windows.Forms.TabPage();
            this.add_img = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.add_btn = new System.Windows.Forms.PictureBox();
            this.remove_btn = new System.Windows.Forms.PictureBox();
            this.posible_list = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.all_list = new System.Windows.Forms.ListBox();
            this.additional_list = new System.Windows.Forms.ListBox();
            this.unique = new System.Windows.Forms.TabPage();
            this.unique_img = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.all_unique_list = new System.Windows.Forms.ListBox();
            this.unique_obj_list = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.settings.SuspendLayout();
            this.standart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.standart_img)).BeginInit();
            this.additional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.add_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.add_btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.remove_btn)).BeginInit();
            this.unique.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unique_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(262, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "НОВЫЙ КОРИДОР";
            // 
            // settings
            // 
            this.settings.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.settings.Controls.Add(this.standart);
            this.settings.Controls.Add(this.additional);
            this.settings.Controls.Add(this.unique);
            this.settings.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settings.Location = new System.Drawing.Point(1, 52);
            this.settings.Name = "settings";
            this.settings.SelectedIndex = 0;
            this.settings.ShowToolTips = true;
            this.settings.Size = new System.Drawing.Size(642, 279);
            this.settings.TabIndex = 1;
            // 
            // standart
            // 
            this.standart.BackColor = System.Drawing.Color.DarkGray;
            this.standart.Controls.Add(this.standart_img);
            this.standart.Controls.Add(this.label6);
            this.standart.Controls.Add(this.label5);
            this.standart.Controls.Add(this.holder_btn);
            this.standart.Controls.Add(this.holder_txt);
            this.standart.Controls.Add(this.holder_lbl);
            this.standart.Controls.Add(this.top_btn);
            this.standart.Controls.Add(this.top_txt);
            this.standart.Controls.Add(this.top_lbl);
            this.standart.Controls.Add(this.crep_btn);
            this.standart.Controls.Add(this.crep_txt);
            this.standart.Controls.Add(this.crep_lbl);
            this.standart.Controls.Add(this.floor_btn);
            this.standart.Controls.Add(this.floor_txt);
            this.standart.Controls.Add(this.floor_lbl);
            this.standart.Controls.Add(this.choose_list);
            this.standart.Controls.Add(this.walls_btn);
            this.standart.Controls.Add(this.walls_txt);
            this.standart.Controls.Add(this.walls_lbl);
            this.standart.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.standart.Location = new System.Drawing.Point(4, 29);
            this.standart.Name = "standart";
            this.standart.Padding = new System.Windows.Forms.Padding(3);
            this.standart.Size = new System.Drawing.Size(634, 246);
            this.standart.TabIndex = 0;
            this.standart.Text = "Стандартные";
            // 
            // standart_img
            // 
            this.standart_img.Location = new System.Drawing.Point(450, 33);
            this.standart_img.Name = "standart_img";
            this.standart_img.Size = new System.Drawing.Size(169, 184);
            this.standart_img.TabIndex = 19;
            this.standart_img.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(487, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "внешний вид:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(284, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "возможные обьекты:";
            // 
            // holder_btn
            // 
            this.holder_btn.Location = new System.Drawing.Point(151, 198);
            this.holder_btn.Name = "holder_btn";
            this.holder_btn.Size = new System.Drawing.Size(73, 23);
            this.holder_btn.TabIndex = 16;
            this.holder_btn.Text = "изменить";
            this.holder_btn.UseVisualStyleBackColor = true;
            this.holder_btn.Click += new System.EventHandler(this.holder_btn_Click);
            // 
            // holder_txt
            // 
            this.holder_txt.AutoSize = true;
            this.holder_txt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.holder_txt.Location = new System.Drawing.Point(8, 225);
            this.holder_txt.Name = "holder_txt";
            this.holder_txt.Size = new System.Drawing.Size(59, 17);
            this.holder_txt.TabIndex = 15;
            this.holder_txt.Text = "Выбрать";
            // 
            // holder_lbl
            // 
            this.holder_lbl.AutoSize = true;
            this.holder_lbl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.holder_lbl.Location = new System.Drawing.Point(8, 200);
            this.holder_lbl.Name = "holder_lbl";
            this.holder_lbl.Size = new System.Drawing.Size(83, 17);
            this.holder_lbl.TabIndex = 14;
            this.holder_lbl.Text = "Держатель:";
            // 
            // top_btn
            // 
            this.top_btn.Location = new System.Drawing.Point(151, 152);
            this.top_btn.Name = "top_btn";
            this.top_btn.Size = new System.Drawing.Size(73, 23);
            this.top_btn.TabIndex = 13;
            this.top_btn.Text = "изменить";
            this.top_btn.UseVisualStyleBackColor = true;
            this.top_btn.Click += new System.EventHandler(this.top_btn_Click);
            // 
            // top_txt
            // 
            this.top_txt.AutoSize = true;
            this.top_txt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.top_txt.Location = new System.Drawing.Point(8, 179);
            this.top_txt.Name = "top_txt";
            this.top_txt.Size = new System.Drawing.Size(59, 17);
            this.top_txt.TabIndex = 12;
            this.top_txt.Text = "Выбрать";
            // 
            // top_lbl
            // 
            this.top_lbl.AutoSize = true;
            this.top_lbl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.top_lbl.Location = new System.Drawing.Point(8, 154);
            this.top_lbl.Name = "top_lbl";
            this.top_lbl.Size = new System.Drawing.Size(67, 17);
            this.top_lbl.TabIndex = 11;
            this.top_lbl.Text = "Потолок:";
            // 
            // crep_btn
            // 
            this.crep_btn.Location = new System.Drawing.Point(151, 101);
            this.crep_btn.Name = "crep_btn";
            this.crep_btn.Size = new System.Drawing.Size(73, 23);
            this.crep_btn.TabIndex = 10;
            this.crep_btn.Text = "изменить";
            this.crep_btn.UseVisualStyleBackColor = true;
            this.crep_btn.Click += new System.EventHandler(this.crep_btn_Click);
            // 
            // crep_txt
            // 
            this.crep_txt.AutoSize = true;
            this.crep_txt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crep_txt.Location = new System.Drawing.Point(8, 128);
            this.crep_txt.Name = "crep_txt";
            this.crep_txt.Size = new System.Drawing.Size(59, 17);
            this.crep_txt.TabIndex = 9;
            this.crep_txt.Text = "Выбрать";
            // 
            // crep_lbl
            // 
            this.crep_lbl.AutoSize = true;
            this.crep_lbl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crep_lbl.Location = new System.Drawing.Point(8, 103);
            this.crep_lbl.Name = "crep_lbl";
            this.crep_lbl.Size = new System.Drawing.Size(50, 17);
            this.crep_lbl.TabIndex = 8;
            this.crep_lbl.Text = "Крепь:";
            // 
            // floor_btn
            // 
            this.floor_btn.Location = new System.Drawing.Point(151, 54);
            this.floor_btn.Name = "floor_btn";
            this.floor_btn.Size = new System.Drawing.Size(73, 23);
            this.floor_btn.TabIndex = 7;
            this.floor_btn.Text = "изменить";
            this.floor_btn.UseVisualStyleBackColor = true;
            this.floor_btn.Click += new System.EventHandler(this.floor_btn_Click);
            // 
            // floor_txt
            // 
            this.floor_txt.AutoSize = true;
            this.floor_txt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.floor_txt.Location = new System.Drawing.Point(8, 81);
            this.floor_txt.Name = "floor_txt";
            this.floor_txt.Size = new System.Drawing.Size(59, 17);
            this.floor_txt.TabIndex = 6;
            this.floor_txt.Text = "Выбрать";
            // 
            // floor_lbl
            // 
            this.floor_lbl.AutoSize = true;
            this.floor_lbl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.floor_lbl.Location = new System.Drawing.Point(8, 56);
            this.floor_lbl.Name = "floor_lbl";
            this.floor_lbl.Size = new System.Drawing.Size(38, 17);
            this.floor_lbl.TabIndex = 5;
            this.floor_lbl.Text = "Пол:";
            // 
            // choose_list
            // 
            this.choose_list.BackColor = System.Drawing.Color.Gray;
            this.choose_list.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.choose_list.FormattingEnabled = true;
            this.choose_list.ItemHeight = 20;
            this.choose_list.Location = new System.Drawing.Point(261, 33);
            this.choose_list.Name = "choose_list";
            this.choose_list.Size = new System.Drawing.Size(169, 184);
            this.choose_list.TabIndex = 3;
            this.choose_list.SelectedIndexChanged += new System.EventHandler(this.choose_list_SelectedIndexChanged);
            // 
            // walls_btn
            // 
            this.walls_btn.Location = new System.Drawing.Point(151, 7);
            this.walls_btn.Name = "walls_btn";
            this.walls_btn.Size = new System.Drawing.Size(73, 23);
            this.walls_btn.TabIndex = 2;
            this.walls_btn.Text = "изменить";
            this.walls_btn.UseVisualStyleBackColor = true;
            this.walls_btn.Click += new System.EventHandler(this.walls_btn_Click);
            // 
            // walls_txt
            // 
            this.walls_txt.AutoSize = true;
            this.walls_txt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.walls_txt.Location = new System.Drawing.Point(8, 34);
            this.walls_txt.Name = "walls_txt";
            this.walls_txt.Size = new System.Drawing.Size(59, 17);
            this.walls_txt.TabIndex = 1;
            this.walls_txt.Text = "Выбрать";
            // 
            // walls_lbl
            // 
            this.walls_lbl.AutoSize = true;
            this.walls_lbl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.walls_lbl.Location = new System.Drawing.Point(8, 9);
            this.walls_lbl.Name = "walls_lbl";
            this.walls_lbl.Size = new System.Drawing.Size(52, 17);
            this.walls_lbl.TabIndex = 0;
            this.walls_lbl.Text = "Стены:";
            // 
            // additional
            // 
            this.additional.BackColor = System.Drawing.Color.DarkGray;
            this.additional.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.additional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.additional.Controls.Add(this.add_img);
            this.additional.Controls.Add(this.label4);
            this.additional.Controls.Add(this.add_btn);
            this.additional.Controls.Add(this.remove_btn);
            this.additional.Controls.Add(this.posible_list);
            this.additional.Controls.Add(this.label3);
            this.additional.Controls.Add(this.all_list);
            this.additional.Controls.Add(this.additional_list);
            this.additional.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.additional.Location = new System.Drawing.Point(4, 29);
            this.additional.Name = "additional";
            this.additional.Padding = new System.Windows.Forms.Padding(3);
            this.additional.Size = new System.Drawing.Size(634, 246);
            this.additional.TabIndex = 1;
            this.additional.Text = "Дополнительные";
            // 
            // add_img
            // 
            this.add_img.Location = new System.Drawing.Point(439, 34);
            this.add_img.Name = "add_img";
            this.add_img.Size = new System.Drawing.Size(169, 204);
            this.add_img.TabIndex = 11;
            this.add_img.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(474, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "внешний вид:";
            // 
            // add_btn
            // 
            this.add_btn.BackColor = System.Drawing.Color.Transparent;
            this.add_btn.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerW;
            this.add_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.add_btn.Location = new System.Drawing.Point(193, 127);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(53, 75);
            this.add_btn.TabIndex = 9;
            this.add_btn.TabStop = false;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // remove_btn
            // 
            this.remove_btn.BackColor = System.Drawing.Color.Transparent;
            this.remove_btn.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerE;
            this.remove_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.remove_btn.Location = new System.Drawing.Point(193, 56);
            this.remove_btn.Name = "remove_btn";
            this.remove_btn.Size = new System.Drawing.Size(53, 75);
            this.remove_btn.TabIndex = 8;
            this.remove_btn.TabStop = false;
            this.remove_btn.Click += new System.EventHandler(this.remove_btn_Click);
            // 
            // posible_list
            // 
            this.posible_list.AutoSize = true;
            this.posible_list.Location = new System.Drawing.Point(257, 14);
            this.posible_list.Name = "posible_list";
            this.posible_list.Size = new System.Drawing.Size(124, 13);
            this.posible_list.TabIndex = 7;
            this.posible_list.Text = "возможные обьекты:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "выбраные обьекты:";
            // 
            // all_list
            // 
            this.all_list.BackColor = System.Drawing.Color.Gray;
            this.all_list.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.all_list.FormattingEnabled = true;
            this.all_list.ItemHeight = 20;
            this.all_list.Location = new System.Drawing.Point(245, 34);
            this.all_list.Name = "all_list";
            this.all_list.Size = new System.Drawing.Size(169, 204);
            this.all_list.TabIndex = 5;
            this.all_list.DoubleClick += new System.EventHandler(this.all_list_DoubleClick);
            // 
            // additional_list
            // 
            this.additional_list.BackColor = System.Drawing.Color.Gray;
            this.additional_list.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.additional_list.FormattingEnabled = true;
            this.additional_list.ItemHeight = 20;
            this.additional_list.Location = new System.Drawing.Point(25, 34);
            this.additional_list.Name = "additional_list";
            this.additional_list.Size = new System.Drawing.Size(169, 204);
            this.additional_list.TabIndex = 4;
            this.additional_list.DoubleClick += new System.EventHandler(this.additional_list_DoubleClick);
            // 
            // unique
            // 
            this.unique.BackColor = System.Drawing.Color.DarkGray;
            this.unique.Controls.Add(this.unique_img);
            this.unique.Controls.Add(this.label7);
            this.unique.Controls.Add(this.pictureBox2);
            this.unique.Controls.Add(this.pictureBox3);
            this.unique.Controls.Add(this.label8);
            this.unique.Controls.Add(this.label9);
            this.unique.Controls.Add(this.all_unique_list);
            this.unique.Controls.Add(this.unique_obj_list);
            this.unique.Location = new System.Drawing.Point(4, 29);
            this.unique.Name = "unique";
            this.unique.Padding = new System.Windows.Forms.Padding(3);
            this.unique.Size = new System.Drawing.Size(634, 246);
            this.unique.TabIndex = 2;
            this.unique.Text = "Уникальные";
            // 
            // unique_img
            // 
            this.unique_img.Location = new System.Drawing.Point(439, 34);
            this.unique_img.Name = "unique_img";
            this.unique_img.Size = new System.Drawing.Size(169, 204);
            this.unique_img.TabIndex = 19;
            this.unique_img.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(474, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "внешний вид:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerW;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(193, 127);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(53, 75);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerE;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(193, 56);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(53, 75);
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(257, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "возможные обьекты:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(37, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "выбраные обьекты:";
            // 
            // all_unique_list
            // 
            this.all_unique_list.BackColor = System.Drawing.Color.Gray;
            this.all_unique_list.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.all_unique_list.FormattingEnabled = true;
            this.all_unique_list.ItemHeight = 20;
            this.all_unique_list.Location = new System.Drawing.Point(245, 34);
            this.all_unique_list.Name = "all_unique_list";
            this.all_unique_list.Size = new System.Drawing.Size(169, 204);
            this.all_unique_list.TabIndex = 13;
            // 
            // unique_obj_list
            // 
            this.unique_obj_list.BackColor = System.Drawing.Color.Gray;
            this.unique_obj_list.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unique_obj_list.FormattingEnabled = true;
            this.unique_obj_list.ItemHeight = 20;
            this.unique_obj_list.Location = new System.Drawing.Point(25, 34);
            this.unique_obj_list.Name = "unique_obj_list";
            this.unique_obj_list.Size = new System.Drawing.Size(169, 204);
            this.unique_obj_list.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(267, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "НАСТРОЙКИ ОБЪЕКТОВ:";
            // 
            // save_btn
            // 
            this.save_btn.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save_btn.Location = new System.Drawing.Point(266, 337);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(150, 30);
            this.save_btn.TabIndex = 3;
            this.save_btn.Text = "СОХРАНИТЬ";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // AddCoridorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(645, 374);
            this.ControlBox = false;
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "AddCoridorForm";
            this.Text = "СОЗДАТЬ КОРИДОР";
            this.settings.ResumeLayout(false);
            this.standart.ResumeLayout(false);
            this.standart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.standart_img)).EndInit();
            this.additional.ResumeLayout(false);
            this.additional.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.add_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.add_btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.remove_btn)).EndInit();
            this.unique.ResumeLayout(false);
            this.unique.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unique_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl settings;
        private System.Windows.Forms.TabPage standart;
        private System.Windows.Forms.TabPage additional;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button holder_btn;
        private System.Windows.Forms.Label holder_txt;
        private System.Windows.Forms.Label holder_lbl;
        private System.Windows.Forms.Button top_btn;
        private System.Windows.Forms.Label top_txt;
        private System.Windows.Forms.Label top_lbl;
        private System.Windows.Forms.Button crep_btn;
        private System.Windows.Forms.Label crep_txt;
        private System.Windows.Forms.Label crep_lbl;
        private System.Windows.Forms.Button floor_btn;
        private System.Windows.Forms.Label floor_txt;
        private System.Windows.Forms.Label floor_lbl;
        private System.Windows.Forms.ListBox choose_list;
        private System.Windows.Forms.Button walls_btn;
        private System.Windows.Forms.Label walls_txt;
        private System.Windows.Forms.Label walls_lbl;
        private System.Windows.Forms.Label posible_list;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox all_list;
        private System.Windows.Forms.ListBox additional_list;
        private System.Windows.Forms.PictureBox remove_btn;
        private System.Windows.Forms.PictureBox add_btn;
        private System.Windows.Forms.TabPage unique;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox add_img;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox standart_img;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox unique_img;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox all_unique_list;
        private System.Windows.Forms.ListBox unique_obj_list;
    }
}