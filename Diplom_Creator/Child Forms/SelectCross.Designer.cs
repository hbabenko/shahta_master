﻿namespace Diplom_Creator
{
    partial class SelectCross
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.West_img = new System.Windows.Forms.PictureBox();
            this.East_img = new System.Windows.Forms.PictureBox();
            this.South_img = new System.Windows.Forms.PictureBox();
            this.North_img = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.West_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.East_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.South_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.North_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // West_img
            // 
            this.West_img.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerW;
            this.West_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.West_img.Location = new System.Drawing.Point(38, 83);
            this.West_img.Name = "West_img";
            this.West_img.Size = new System.Drawing.Size(60, 75);
            this.West_img.TabIndex = 3;
            this.West_img.TabStop = false;
            this.West_img.Click += new System.EventHandler(this.West_img_Click);
            this.West_img.MouseLeave += new System.EventHandler(this.West_img_MouseLeave);
            this.West_img.MouseHover += new System.EventHandler(this.West_img_MouseHover);
            // 
            // East_img
            // 
            this.East_img.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerE;
            this.East_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.East_img.Location = new System.Drawing.Point(186, 83);
            this.East_img.Name = "East_img";
            this.East_img.Size = new System.Drawing.Size(60, 75);
            this.East_img.TabIndex = 2;
            this.East_img.TabStop = false;
            this.East_img.Click += new System.EventHandler(this.East_img_Click);
            this.East_img.MouseLeave += new System.EventHandler(this.East_img_MouseLeave);
            this.East_img.MouseHover += new System.EventHandler(this.East_img_MouseHover);
            // 
            // South_img
            // 
            this.South_img.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerS;
            this.South_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.South_img.Location = new System.Drawing.Point(105, 165);
            this.South_img.Name = "South_img";
            this.South_img.Size = new System.Drawing.Size(75, 60);
            this.South_img.TabIndex = 1;
            this.South_img.TabStop = false;
            this.South_img.Click += new System.EventHandler(this.South_img_Click);
            this.South_img.MouseLeave += new System.EventHandler(this.South_img_MouseLeave);
            this.South_img.MouseHover += new System.EventHandler(this.South_img_MouseHover);
            // 
            // North_img
            // 
            this.North_img.BackgroundImage = global::Diplom_Creator.Properties.Resources.PointerN;
            this.North_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.North_img.Location = new System.Drawing.Point(105, 22);
            this.North_img.Name = "North_img";
            this.North_img.Size = new System.Drawing.Size(75, 60);
            this.North_img.TabIndex = 0;
            this.North_img.TabStop = false;
            this.North_img.Click += new System.EventHandler(this.North_img_Click);
            this.North_img.MouseLeave += new System.EventHandler(this.North_img_MouseLeave);
            this.North_img.MouseHover += new System.EventHandler(this.North_img_MouseHover);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::Diplom_Creator.Properties.Resources.point_cross;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(115, 94);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(54, 56);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // SelectCross
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(296, 238);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.West_img);
            this.Controls.Add(this.East_img);
            this.Controls.Add(this.South_img);
            this.Controls.Add(this.North_img);
            this.Name = "SelectCross";
            ((System.ComponentModel.ISupportInitialize)(this.West_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.East_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.South_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.North_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox North_img;
        private System.Windows.Forms.PictureBox South_img;
        private System.Windows.Forms.PictureBox East_img;
        private System.Windows.Forms.PictureBox West_img;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}