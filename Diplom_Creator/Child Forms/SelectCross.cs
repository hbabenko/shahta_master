﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{
    public partial class SelectCross : Form
    {
        public delegate void SelectCrossHandler(Joint_m.BuildDirection selected_direction);
        //В Формах только РИСОВАНИЕ и реакции на СОБЫТИЯ! 
        //Все логика в дополнительных модулях или обьекатах
        //Events
        public static event SelectCrossHandler CrossTypeSelected;
        public SelectCross()
        {
            InitializeComponent();
        }
        public void InitWithClosedDirection(List<Joint_m.BuildDirection> closed_directions)
        {
            if (closed_directions.Contains(Joint_m.BuildDirection.aEast))
            {
                East_img.BackgroundImage = Properties.Resources.PointerEN;
                East_img.Enabled = false;
            }
            else
            {
                East_img.BackgroundImage = Properties.Resources.PointerE;
                East_img.Enabled = true;
            }

            if (closed_directions.Contains(Joint_m.BuildDirection.aNorth))
            {
                North_img.BackgroundImage = Properties.Resources.PointerNN;
                North_img.Enabled = false;
            }
            else
            {
                North_img.BackgroundImage = Properties.Resources.PointerN;
                North_img.Enabled = true;
            }

            if (closed_directions.Contains(Joint_m.BuildDirection.aSouth))
            {
                South_img.BackgroundImage = Properties.Resources.PointerSN;
                South_img.Enabled = false;
            }
            else
            {
                South_img.BackgroundImage = Properties.Resources.PointerS;
                South_img.Enabled = true;
            }

            if (closed_directions.Contains(Joint_m.BuildDirection.aWest))
            {
                West_img.BackgroundImage = Properties.Resources.PointerWN;
                West_img.Enabled = false;
            }
            else
            {
                West_img.BackgroundImage = Properties.Resources.PointerW;
                West_img.Enabled = true;
            }

        }

        private void North_img_Click(object sender, EventArgs e)
        {
            CrossTypeSelected(Joint_m.BuildDirection.aNorth);
            this.Hide();
        }
        private void East_img_Click(object sender, EventArgs e)
        {
            CrossTypeSelected(Joint_m.BuildDirection.aEast);
            this.Hide();
        }

        private void South_img_Click(object sender, EventArgs e)
        {
            CrossTypeSelected(Joint_m.BuildDirection.aSouth);
            this.Hide();
        }

        private void West_img_Click(object sender, EventArgs e)
        {
            CrossTypeSelected(Joint_m.BuildDirection.aWest);
            this.Hide();
        }





        private void South_img_MouseHover(object sender, EventArgs e)
        {
            South_img.BackgroundImage = Properties.Resources.PointerSO;
        }

        private void North_img_MouseHover(object sender, EventArgs e)
        {
            North_img.BackgroundImage = Properties.Resources.PointerNO;
        }

        private void North_img_MouseLeave(object sender, EventArgs e)
        {
            North_img.BackgroundImage = Properties.Resources.PointerN;
        }

        private void East_img_MouseHover(object sender, EventArgs e)
        {
            East_img.BackgroundImage = Properties.Resources.PointerEO;
        }

        private void East_img_MouseLeave(object sender, EventArgs e)
        {
            East_img.BackgroundImage = Properties.Resources.PointerE;
        }

        private void South_img_MouseLeave(object sender, EventArgs e)
        {
            South_img.BackgroundImage = Properties.Resources.PointerS;
        }

        private void West_img_MouseHover(object sender, EventArgs e)
        {
            West_img.BackgroundImage = Properties.Resources.PointerWO;
        }

        private void West_img_MouseLeave(object sender, EventArgs e)
        {
            West_img.BackgroundImage = Properties.Resources.PointerW;
        }

    }
}
