﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{
    //часть для обработки перекрестков и рекции на их нажатие
     public partial class MainForm : Form
    {
         Diplom_Creator.SelectCross SelectACross = new SelectCross();
         private void SelectCross_CrossTypeSelected(Joint_m.BuildDirection selected_direction)
         {
             CurrentDirection = selected_direction;
            // Console.WriteLine("Current Direction " + CurrentDirection);
             Point_m.setStandartPointLimitation(Point_m.PointType.aCross_point, CurrentDirection);
             KoridorForm.newKoridor = new Koridor_m(Current_Koridor_id, 0);
             foreach (Point_m.PointLimitition limit in Point_m.CurrentPointLimit)
             {
                // Console.WriteLine("New Limitition " + limit);
             }
             KoridorForm.Show();
         }
    }
}
