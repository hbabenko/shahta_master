﻿namespace Diplom_Creator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tool_lbl1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tool_lbl2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.проектToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выйтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.X_Kurs = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.Y_Kurs = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.create_btn = new System.Windows.Forms.ToolStripButton();
            this.edit_btn = new System.Windows.Forms.ToolStripButton();
            this.save_btn = new System.Windows.Forms.ToolStripButton();
            this.delete_btn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.library_btn = new System.Windows.Forms.ToolStripButton();
            this.point_start = new System.Windows.Forms.PictureBox();
            this.MainField = new System.Windows.Forms.PictureBox();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.point_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainField)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool_lbl1,
            this.tool_lbl2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 708);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tool_lbl1
            // 
            this.tool_lbl1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tool_lbl1.Name = "tool_lbl1";
            this.tool_lbl1.Size = new System.Drawing.Size(157, 17);
            this.tool_lbl1.Text = "Масштаб 1 ячейка = 4 м";
            // 
            // tool_lbl2
            // 
            this.tool_lbl2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tool_lbl2.ForeColor = System.Drawing.Color.Maroon;
            this.tool_lbl2.Name = "tool_lbl2";
            this.tool_lbl2.Size = new System.Drawing.Size(289, 17);
            this.tool_lbl2.Text = "       Для завершения коридора нажмите Enter";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.проектToolStripMenuItem,
            this.помощьToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // проектToolStripMenuItem
            // 
            this.проектToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выйтиToolStripMenuItem});
            this.проектToolStripMenuItem.Name = "проектToolStripMenuItem";
            this.проектToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.проектToolStripMenuItem.Text = "Программа";
            // 
            // выйтиToolStripMenuItem
            // 
            this.выйтиToolStripMenuItem.Name = "выйтиToolStripMenuItem";
            this.выйтиToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.выйтиToolStripMenuItem.Text = "Выйти";
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.помощьToolStripMenuItem.Text = "Помощь";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.X_Kurs,
            this.toolStripLabel2,
            this.Y_Kurs,
            this.toolStripSeparator1,
            this.create_btn,
            this.edit_btn,
            this.save_btn,
            this.delete_btn,
            this.toolStripSeparator2,
            this.toolStripLabel4,
            this.toolStripLabel3,
            this.toolStripSeparator3,
            this.toolStripLabel5,
            this.library_btn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(50, 22);
            this.toolStripLabel1.Text = "КУРСОР";
            // 
            // X_Kurs
            // 
            this.X_Kurs.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X_Kurs.Name = "X_Kurs";
            this.X_Kurs.Size = new System.Drawing.Size(29, 22);
            this.X_Kurs.Text = "400";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(12, 22);
            this.toolStripLabel2.Text = ":";
            // 
            // Y_Kurs
            // 
            this.Y_Kurs.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y_Kurs.Name = "Y_Kurs";
            this.Y_Kurs.Size = new System.Drawing.Size(29, 22);
            this.Y_Kurs.Text = "400";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // create_btn
            // 
            this.create_btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.create_btn.Image = global::Diplom_Creator.Properties.Resources.New;
            this.create_btn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.create_btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(79, 22);
            this.create_btn.Text = "toolStripButton1";
            this.create_btn.ToolTipText = "Создать новую точку";
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // edit_btn
            // 
            this.edit_btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.edit_btn.Image = global::Diplom_Creator.Properties.Resources.Edit;
            this.edit_btn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.edit_btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.edit_btn.Name = "edit_btn";
            this.edit_btn.Size = new System.Drawing.Size(79, 22);
            this.edit_btn.Text = "toolStripButton2";
            this.edit_btn.ToolTipText = "Удалить существующие точки";
            this.edit_btn.Click += new System.EventHandler(this.edit_btn_Click);
            // 
            // save_btn
            // 
            this.save_btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.save_btn.Image = global::Diplom_Creator.Properties.Resources.Save;
            this.save_btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(23, 22);
            this.save_btn.Text = "toolStripButton3";
            this.save_btn.ToolTipText = "Сохранить результат";
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // delete_btn
            // 
            this.delete_btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.delete_btn.Image = global::Diplom_Creator.Properties.Resources.Delete;
            this.delete_btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(23, 22);
            this.delete_btn.Text = "toolStripButton4";
            this.delete_btn.ToolTipText = "Удалить результат";
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(0, 22);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(358, 22);
            this.toolStripLabel3.Text = "                                                                                 " +
                "                                    ";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(125, 22);
            this.toolStripLabel5.Text = "ДОБАВИТЬ ОБЬЕКТЫ:";
            // 
            // library_btn
            // 
            this.library_btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.library_btn.Image = global::Diplom_Creator.Properties.Resources.Library2;
            this.library_btn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.library_btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.library_btn.Name = "library_btn";
            this.library_btn.Size = new System.Drawing.Size(144, 22);
            this.library_btn.Text = "toolStripButton5";
            this.library_btn.ToolTipText = "Выбрать обьекты из библиотеки";
            this.library_btn.Click += new System.EventHandler(this.library_btn_Click);
            // 
            // point_start
            // 
            this.point_start.BackColor = System.Drawing.Color.Transparent;
            this.point_start.Image = global::Diplom_Creator.Properties.Resources.point_beginS;
            this.point_start.Location = new System.Drawing.Point(215, 326);
            this.point_start.Name = "point_start";
            this.point_start.Size = new System.Drawing.Size(15, 15);
            this.point_start.TabIndex = 5;
            this.point_start.TabStop = false;
            this.point_start.Visible = false;
            this.point_start.Click += new System.EventHandler(this.point_start_Click);
            // 
            // MainField
            // 
            this.MainField.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.MainField.Image = global::Diplom_Creator.Properties.Resources.Back;
            this.MainField.Location = new System.Drawing.Point(0, 52);
            this.MainField.Name = "MainField";
            this.MainField.Size = new System.Drawing.Size(1000, 650);
            this.MainField.TabIndex = 4;
            this.MainField.TabStop = false;
            this.MainField.Paint += new System.Windows.Forms.PaintEventHandler(this.MainField_Paint);
            this.MainField.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainField_MouseDown);
            this.MainField.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainField_MouseMove);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.point_start);
            this.Controls.Add(this.MainField);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "КОНСТРУКТОР";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.point_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tool_lbl1;
        private System.Windows.Forms.ToolStripStatusLabel tool_lbl2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem проектToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выйтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel X_Kurs;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel Y_Kurs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton create_btn;
        private System.Windows.Forms.ToolStripButton edit_btn;
        private System.Windows.Forms.ToolStripButton save_btn;
        private System.Windows.Forms.ToolStripButton delete_btn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.PictureBox MainField;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripButton library_btn;
        private System.Windows.Forms.PictureBox point_start;
    }
}

