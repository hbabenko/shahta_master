﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{
    public partial class MainForm : Form
    {
       

        Joint_m.BuildDirection CurrentDirection;
        
        List<Point_m> Points;
        //Отступ для центрирования точки
       
        //Текущие координаты точки 
        int CoordX;
        int CoordY;
        //Координаты курсора мыши
        int CellNumberX;
        int CellNumberY;

        //Режим - добавить новый коридор
        bool addPointsMode;
        //Режим - редактировать коридор
        bool editPointsMode;
        //Режим отрисовки привязки
        bool drawLine;

        int NewPointTag;
        //Дополнительные окна
        //  Создание новой точки

        Diplom_Creator.AddPointForm AddAPoint = new Diplom_Creator.AddPointForm();
        //  Создание нового коридора
        private Point PointLocation()
        {

           // Console.WriteLine("LINE : " + CoordX + " " + CoordY);
            int LocalXPoint = Point_m.XtoPixel(CoordX) + MainField.Location.X;
            int LocalYPoint = Point_m.YtoPixel(CoordY) + MainField.Location.Y;
          //  Console.WriteLine("LINE 2 : " + LocalXPoint + " " + LocalYPoint);
            Point answer = new Point(LocalXPoint, LocalYPoint);
            return answer;

        }
        

       
        private void setCurrentDirection(Point_m.PointType newPointType)
        {
            if (newPointType == Point_m.PointType.aRightConor_point)
            {
                switch(CurrentDirection)
                {
                    case Joint_m.BuildDirection.aNorth:
                         {
                             CurrentDirection = Joint_m.BuildDirection.aEast;
                             break;
                         }
                    case Joint_m.BuildDirection.aEast:
                         {
                             CurrentDirection = Joint_m.BuildDirection.aSouth;
                             break;
                         }
                    case Joint_m.BuildDirection.aSouth:
                         {
                             CurrentDirection = Joint_m.BuildDirection.aWest;
                             break;
                         }
                    case Joint_m.BuildDirection.aWest:
                         {
                             CurrentDirection = Joint_m.BuildDirection.aNorth;
                             break;
                         }
                }
                
            }
            if (newPointType == Point_m.PointType.aLeftConor_point)
            {
                switch (CurrentDirection)
                {
                    case Joint_m.BuildDirection.aNorth:
                        {
                            CurrentDirection = Joint_m.BuildDirection.aWest;
                            break;
                        }
                    case Joint_m.BuildDirection.aEast:
                        {
                            CurrentDirection = Joint_m.BuildDirection.aNorth;
                            break;
                        }
                    case Joint_m.BuildDirection.aSouth:
                        {
                            CurrentDirection = Joint_m.BuildDirection.aEast;
                            break;
                        }
                    case Joint_m.BuildDirection.aWest:
                        {
                            CurrentDirection = Joint_m.BuildDirection.aSouth;
                            break;
                        }
                }

            }
            //Во всех других случаях направление остается то же

        }
        // Начальная инициализация
        private void ClearPointVariables()
        {
            addPointsMode = false;
            editPointsMode = false;
            drawLine = false;
            NewPointTag = 0;
            CoordX = 0;
            CoordY = 0;
            CellNumberX = 0;
            CellNumberY = 0;
            Point_m.CurrentPointId = 0;
            CurrentDirection = Joint_m.BuildDirection.aNorth;
            Point_m.CurrentPointLimit = new List<Point_m.PointLimitition>();
            Points = new List<Point_m>();
            Point_m.CurrentPointLimit.Add(Point_m.PointLimitition.aNoLimit);
        }
        //
        private void AddButtonForm_AddPoint(Point_m.PointType PointSelected)
        {
            
           // Console.WriteLine("Selected " + PointSelected);
           // Console.WriteLine("Points " + Points.Count);
            //выбрана точка!
            if (Points.Count < 1)
                Points.Add(new Point_m(CoordX, CoordY));
            else
                Points.Add(new Point_m(CoordX, CoordY,Points.First(),PointSelected));
 
            //Надо добавить ее на сетку
            this.AddAPointToTheGrid(PointSelected);
            //Сменить направление и ограничения
            setCurrentDirection(PointSelected);
            Point_m.setStandartPointLimitation(PointSelected, CurrentDirection);
            //Вызвать создание коридора, если тип точки позволяет
            drawLine = true;
            //ставим начальную точку
            //включаем привязку привязку
            if (PointSelected == Point_m.PointType.aCross_point)
            {
                //Сначала создадим перекресток
                //Если перекресток посреди пути, то можно перемещатся только прямо                    
                //Поставим на это ограничения
                SelectACross.InitWithClosedDirection(Joint_m.onlyStraightDirection(CurrentDirection));
                SelectACross.Show();
            }
            else
            {
                KoridorForm.newKoridor = new Koridor_m(Current_Koridor_id, 0);
                KoridorForm.Show();
            }

        }


        private void AddAPointToTheGrid(Point_m.PointType PointSelected)
        {

            PictureBox newPoint = new PictureBox();
            newPoint.Name = "points_" + NewPointTag;
            //newPoint.BackColor = System.Drawing.SystemColors
            newPoint.Image = Point_m.PointImage(PointSelected); 
            newPoint.Size = this.point_start.Size;
            newPoint.Location = this.PointLocation();
            newPoint.Tag = Points.Last().Id_m();
            newPoint.MouseDown += new System.Windows.Forms.MouseEventHandler(this.point_start_Click);

            //Add  action to a new module
            this.Controls.Add(newPoint);
            NewPointTag++;
            newPoint.BringToFront();
        }
        //Обработка собітия нажатия на точку
        private void point_start_Click(object sender, EventArgs e)
        {
           // Console.WriteLine("Point Click");
            if(addPointsMode)
            {
                 System.Windows.Forms.PictureBox tempPicture;
                tempPicture = (System.Windows.Forms.PictureBox)sender;
                string SelectedModuleName = tempPicture.Name;
                int SelectedId =(int)tempPicture.Tag;
                Point_m SelectedPoint = Points.Find(x => x.Id_m() == SelectedId);
                if (SelectedPoint.pointType() == Point_m.PointType.aCross_point)
                {
                  //  Console.WriteLine("Select object" + SelectedModuleName + " " + SelectedPoint.X_m() + " " + SelectedPoint.Y_m());
                    CoordX = SelectedPoint.X_m();
                    CoordY = SelectedPoint.Y_m();
                    Points.Add(SelectedPoint);
                    drawLine = true;

                    //Найдем разрешенные направления
                    List<Joint_m.BuildDirection> ForbittenDirections = Joint_m.findClosedDirectionsById(Joints, SelectedPoint.Id_m());
                   //Если не все направления еще закрыты
                    if (ForbittenDirections.Count < 4)
                    {
                        SelectACross.InitWithClosedDirection(ForbittenDirections);
                        SelectACross.Show();
                    }
                }
                
            }
        }
    }
}