﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{
    public partial class MainForm : Form
    {
        List<Koridor_m> Koridors;
        Diplom_Creator.AddCoridorForm KoridorForm = new Diplom_Creator.AddCoridorForm();
        int Current_Koridor_id;

        public void ClearKoridorVariables()
        {
            Current_Koridor_id = 0;
            Joint_m.CurrentJointId = 0;
            Koridors = new List<Koridor_m>();
        }
        private void AddCoridorForm_AddKoridor(Koridor_m _Koridor)
        {
            this.Enabled = true;
            //if its first point
            
            Koridors.Add(_Koridor);
            Console.WriteLine("Current koridor ALL  " + _Koridor.Id_m() + " static" + Current_Koridor_id);
            if (Points.Count > 1)
            {
                int lastIndex = Points.Count - 1;
                if (Points[lastIndex - 1].pointType() != Point_m.PointType.aEnd_point)
                {
                    //Если мі рисуем новій коридор
                    double distance = Point_m.findDistance(Points[lastIndex - 1], Points[lastIndex]);
                    Koridors[Current_Koridor_id - 1].setLength(distance);
                    // set length Koridors[lastIndex]
                    Console.WriteLine("KOridor Length " + distance);
                    Console.WriteLine("Current koridor " + _Koridor.Id_m());
                    Joint_m new_joint = new Joint_m(Koridors[Current_Koridor_id-1], Points[lastIndex - 1], Points[lastIndex]);
                    Joints.Add(new_joint);

                }

                if (Points[lastIndex].pointType() == Point_m.PointType.aEnd_point)
                {
                    //Если продолжаем рисовать
                    addPointsMode = false;
                    create_btn.Enabled = true;
                    tool_lbl2.Text = "";
                    drawLine = false;

                }

            }
            Current_Koridor_id++;
            
           
           
            // if we already have at least one koridor and point

            
        }

    }
}