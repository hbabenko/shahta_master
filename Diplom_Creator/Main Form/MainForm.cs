﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diplom_Creator
{

    public partial class MainForm : Form
    {
        //Выставляем размеры сетки
        //Размеры ячеек
        //const int kCellHeight = 10;
        //const int kCellWight = 10;
        //Количество ячеек
        const int kCellCountW = 100;
        const int kCellCountH = 65;
       
        //Схема состоит из точек и связывающих их коридоров
        //Один коридор строит одинаковые модули. Их количество определяется длинной модуля.

        List<Joint_m> Joints;
        //Инициализация переменных 
        private void ClearVariables()
        {
            this.tool_lbl2.Text = "";
            Joints = new List<Joint_m>();
            ClearPointVariables();
            ClearKoridorVariables();
        }
        //Инициализация констант и настройка системы
        private void InitAll()
        {
    
            //Делегаты - передают данные от определенной формы
            //Делегат "добавить новую точку"
            AddPointForm.AddPoint += new AddPointForm.AddPointHandler(AddButtonForm_AddPoint);
            AddCoridorForm.AddKoridor +=new AddCoridorForm.AddKoridorHandler(AddCoridorForm_AddKoridor);
            SelectCross.CrossTypeSelected += new SelectCross.SelectCrossHandler(SelectCross_CrossTypeSelected);
        }
       
        public MainForm()
        {
            this.InitAll();
            
            InitializeComponent();

            this.ClearVariables();
        }

        private void MainField_MouseMove(object sender, MouseEventArgs e)
        {

            CellNumberX =  Point_m.PixeltoX(e.X);
            CellNumberY = Point_m.PixeltoY(e.Y);

            //Показываем текущее положение курсора на экране
            if (CellNumberX<10)
                this.X_Kurs.Text = "0" + CellNumberX;
            else
                this.X_Kurs.Text = "" + CellNumberX;
            if (CellNumberY < 10)
                 this.Y_Kurs.Text = "0" + CellNumberY;
            else
                 this.Y_Kurs.Text = "" + CellNumberY;

            if (drawLine)
            {
                MainField.Invalidate();
            }
           
        }

        private void MainField_MouseDown(object sender, MouseEventArgs e)
        {
            //реагируем на клик мыши
            if (addPointsMode)
            {

                int newX = Point_m.PixeltoX(e.X);
                int newY = Point_m.PixeltoY(e.Y);
                if (Point_m.canWeCreatePoint(newX, newY,CoordX,CoordY)){
                    //Заблокировать основную форму пока выбирается точка
                    this.Enabled = false;
                    //Вызвать диалоговое окно создания точки и коридора
                   // Console.WriteLine("Add a point and a coridor");
                    CoordX = newX;
                    CoordY = newY;
                    if (Points.Count < 1)
                        AddAPoint.createFirstPoint();
                    else
                        AddAPoint.createNextPoints();
                    AddAPoint.Show();
                    tool_lbl2.Text = "Режим редактирования.Нажмите Enter для завершения.";

                }else
                {
                    tool_lbl2.Text = "Нельзя поставить точку в этом месте";

                }
               
            }
        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            //Создать коридор
            if (!addPointsMode)
            {
                addPointsMode = true;
                create_btn.Enabled = false;
                tool_lbl2.Text = "Путь должен оканчитваться тупиком.";
            }
        }

        private void edit_btn_Click(object sender, EventArgs e)
        {
            //Редактировать коридоры
            editPointsMode = true;
        }


        private void delete_btn_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Clear all mode");
            //Привести все к начальному состоянию
            this.ClearVariables();
        }

        private void library_btn_Click(object sender, EventArgs e)
        {
            //Показать все возможные обьекты
        }

        private void MainField_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.FromArgb(255, 0, 0, 0));
            pen.Color = Color.SlateGray;
            pen.Width = 4;
            //int YCellCenter = this.point_start.Height / 2;
            //int XCellCenter = this.point_start.Width / 2;

            for (int i = 0; i < Points.Count - 1; i++)
            {
                Point_m First = Points[i];
                Point_m Next = Points[i + 1];
                // Console.WriteLine("draw line " + First.Id_m() +" "+ Next.Id_m());
                e.Graphics.DrawLine(pen, First._Point(), Next._Point());
            }
            if (drawLine)
            {
                
               
                e.Graphics.DrawLine(pen, Point_m.XtoCellCenterPixel(CoordX), Point_m.YtoCellCenterPixel(CoordY), Point_m.XtoCellCenterPixel(CellNumberX), Point_m.YtoCellCenterPixel(CellNumberY));
                pen.Dispose();
                
            }
        }

       

    }
}
