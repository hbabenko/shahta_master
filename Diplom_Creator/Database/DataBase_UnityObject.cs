﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;
using System.Data;
using System.Data.SqlClient;

namespace Diplom_Creator
{
    public class DataBase_UnityObject
    {
        
        const string ObjectTableName = "UnityObjects";

        static public UnityObject GetObjectWithId(int Object_id) //getting With of objects
        {
            UnityObject Answer; 
            string strSql = "SELECT * FROM " + ObjectTableName + " WHERE Object_Id =" + Object_id;

            SqlCeConnection _Connection = new SqlCeConnection("Data Source = ObjectsLibrary.sdf; Persist Security Info=False");
            _Connection.Open();

            SqlCeCommand _command = _Connection.CreateCommand();

            _command.CommandText = strSql;

            SqlCeDataReader DataReader = null;
            DataReader = _command.ExecuteReader();

            Answer = new UnityObject();
            while (DataReader.Read())
            {
                string CurrentObjName = (DataReader["Object_Name"].ToString());
                string CurrentObjectType = (DataReader["Object_Type"].ToString());
                string CurrentObjectNamePrefix = (DataReader["Object_Prefix"].ToString());

                Answer.InitObject(Object_id, Convert.ToInt32(CurrentObjectType), CurrentObjName, CurrentObjectNamePrefix);

            }

            _Connection.Close();

            return Answer;

        }
        static public bool hasObjectWithName(string Name, string Prefix)
        {

            List<UnityObject> all_objects = GetAllObjects();

            foreach (UnityObject _object in all_objects)
            {
                if (Equals(_object.Name(), Name))
                {
                   
                    if (Equals(_object.Prefix(), Prefix))
                    {
                        
                        return true;
                    }
                }
            }

            return false; ;

        }
        
        static public List<UnityObject> GetObjectWithType(UnityObject.ObjectType type)
        {
            List<UnityObject> Answer = new List<UnityObject>();
            // ObjectsDataBaseDataSet _DataSet = new ObjectsDataBaseDataSet();
            string strSql = "SELECT * FROM " + ObjectTableName + " WHERE Object_Type =" + (int)type;

            SqlCeConnection _Connection = new SqlCeConnection("Data Source = ObjectsLibrary.sdf; Persist Security Info=False");
            _Connection.Open();

            SqlCeCommand _command = _Connection.CreateCommand();

            _command.CommandText = strSql;

            SqlCeDataReader DataReader = null;
            DataReader = _command.ExecuteReader();

            while (DataReader.Read())
            {
                string CurrentObjId = (DataReader["Object_ID"].ToString());
                int Object_id = Convert.ToInt32(CurrentObjId);
                string CurrentObjName = (DataReader["Object_Name"].ToString());
                string CurrentObjectType = (DataReader["Object_Type"].ToString());
                string CurrentObjectNamePrefix = (DataReader["Object_Prefix"].ToString());

                UnityObject newObject = new UnityObject();
                newObject.InitObject(Object_id, Convert.ToInt32(CurrentObjectType), CurrentObjName, CurrentObjectNamePrefix);

                Answer.Add(newObject);
            }

            _Connection.Close();
            return Answer;

        }
        static public List<UnityObject> GetAllObjects()
        {
            List<UnityObject> Answer = new List<UnityObject>();
            // ObjectsDataBaseDataSet _DataSet = new ObjectsDataBaseDataSet();
            string strSql = "SELECT * FROM " + ObjectTableName;

            SqlCeConnection _Connection = new SqlCeConnection("Data Source = ObjectsLibrary.sdf; Persist Security Info=False");
            _Connection.Open();

            SqlCeCommand _command = _Connection.CreateCommand();

            _command.CommandText = strSql;

            SqlCeDataReader DataReader = null;
            DataReader = _command.ExecuteReader();

            while (DataReader.Read())
            {
                string CurrentObjId = (DataReader["Object_ID"].ToString());
                int Object_id = Convert.ToInt32(CurrentObjId);
                string CurrentObjName = (DataReader["Object_Name"].ToString());
                string CurrentObjectType = (DataReader["Object_Type"].ToString());
                string CurrentObjectNamePrefix = (DataReader["Object_Prefix"].ToString());

                UnityObject newObject = new UnityObject();
                newObject.InitObject(Object_id, Convert.ToInt32(CurrentObjectType), CurrentObjName, CurrentObjectNamePrefix);

                Answer.Add(newObject);
            }

            _Connection.Close();
            return Answer;

        }
    }
}
