﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Diplom_Creator
{
    public class UnityObject
    {
        //class to work with data base
        private int id;
        private int type;
        private string name;
        private string prefix;
        public enum ObjectType
        {
            Animation = 1,
            Custom = 2,
            Walls = 3,
            Floor = 4,
            Crep = 5,
            Top = 6,
            Holder = 7
        };

        public void InitObject(int _id, int _type, string _name, string _prefix)
        {
            id = _id;
            type = _type;
            name = _name;
            prefix = _prefix;
        }
        public int Id()
        {
            return id;
        }
        public int Type()
        {
            return type;
        }
        public string Name()
        {
            return name;
        }
        public string Prefix()
        {
            return prefix;
        }
        public string FullName()
        {
            string fullName;
            if (string.Equals(prefix, ""))
            {  fullName = name;
            }else
                 fullName = name + "_" + prefix;
            return fullName;
        }
    }
}
